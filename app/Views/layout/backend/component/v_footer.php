</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
  <div class="p-3">
    <h5>Title</h5>
    <p>Sidebar content</p>
  </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <!-- <div class="float-right d-none d-sm-inline">
    Anything you want
  </div> -->
  <!-- Default to the left -->
  <strong>Copyright &copy; 2020 <a href="#">Ardinur_03</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery
<script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/jquery/jquery.min.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script> -->
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

<!-- AdminLTE App -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/dist/js/adminlte.min.js'); ?>"></script>
<!-- sweet alert -->
<script src="<?= base_url('assets/js/sweetalert2.all.min.js'); ?>"></script>
<!-- chartJS -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/dist/js/Chart.min.js'); ?>"></script>
<!-- Custom JS -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/js/custom.js'); ?>"></script>
</body>

</html>