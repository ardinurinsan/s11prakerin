<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>

      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown user-menu mr-5">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <img src="<?= (session()->get('Foto') != Null) ? base_url('assets/datausers/img/' . session()->get('Foto')) : base_url('assets/datausers/img/user-default.png') ?>" class="user-image img-circle elevation-2" alt="User Image">
          <span class="d-none d-md-inline"><?= session()->get('Username'); ?></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <!-- User image -->
          <li class="user-header bg-primary">
            <img src="<?= (session()->get('Foto') != Null) ? base_url('assets/datausers/img/' . session()->get('Foto')) : base_url('assets/datausers/img/user-default.png') ?>" class="img-circle elevation-2" alt="User Image">
            <p>
              <?= session()->get('Username'); ?>
              <small>
                <span class="badge badge-success"><?= session()->get('NamaRole'); ?></span>
              </small>
            </p>
          </li>
          <!-- Menu Body -->
          <!-- <li class="user-body">
            <div class="row">
              <div class="col-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-4 text-center">
                <a href="#">Friends</a>
              </div>
            </div> -->
          <!-- /.row -->
      </li>
      <!-- Menu Footer-->
      <li class="user-footer">
        <a href="<?= session()->get('Role') == 1
                    ? '/admin/profile'
                    : (session()->get('Role') == 2
                      ? '/hubin/profile'
                      : (session()->get('Role') == 3
                        ? '/siswa/profile'
                        : ''));
                  ?>" class="btn btn-sm btn-light">Profile</a>
        <a href="<?= route_to('logout'); ?>" class="btn btn-sm btn-light float-right">Sign out</a>
      </li>
    </ul>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a>
    </li>
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <!-- <img src="<?= base_url(); ?>/assets/img/icon-img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3 mt-1" style="opacity: .8"> -->
      <span class="brand-text font-weight-light judul">S11Prakerin</span>
    </a>