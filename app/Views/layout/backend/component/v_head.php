<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?= $title; ?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url('assets/template-dashboard-admin-lte/plugins/fontawesome-free/css/all.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/template-dashboard-admin-lte/dist/css/adminlte.min.css'); ?> ">
  <!-- Bootstrap CSS murni -->
  <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>" crossorigin="anonymous">

  <!-- datatables -->
  <link rel="stylesheet" href="<?= base_url('assets/template-dashboard-admin-lte/plugins/datatables/css/dataTables.bootstrap4.css'); ?>" crossorigin="anonymous">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/template-dashboard-admin-lte/css/style.css') ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- animate css -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
  <!-- my font -->
  <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Nunito+Sans:wght@400;600;800&family=Poppins:wght@400;900&display=swap" rel="stylesheet">

  <!-- DATABLE -->
  <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"> -->
  <link rel=" stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

  <!-- jQuery -->
  <script src=" <?= base_url('assets/template-dashboard-admin-lte/plugins/jquery/jquery.min.js'); ?>">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <!-- Js Datatabes -->
  <script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/datatables/js/jquery.dataTables.js'); ?>"></script>
  <script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/datatables/jsbs4/dataTables.bootstrap4.js'); ?>"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>


</head>


<body class="hold-transition sidebar-mini text-sm">