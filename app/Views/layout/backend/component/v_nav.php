<?php

use CodeIgniter\Session\Session;
use phpDocumentor\Reflection\Types\Null_;
?>
<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user panel (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="<?= (session()->get('Foto') != Null) ? base_url('assets/dataUsers/img/' . session()->get('Foto')) : base_url('assets/dataUsers/img/user-default.png') ?>" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block">
        <?php
        if (session()->get('Role') == 1) {
          echo session()->get('Username');
        } elseif (session()->get('Role') == 2) {
          echo session()->get('Username');
        } elseif (session()->get('Role') == 3) {
          echo session()->get('siswa')->Nama_Siswa;
        }
        ?>
      </a>

      <span class="badge badge-success"><?= session()->get('NamaRole') ?></span>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Dashboard start -->
      <li class="nav-item has-treeview">
        <!-- ekspresi1 ? ekspresi2 : (ekspresi3 ? ekspresi4 : (dst..)) -->
        <a href="
        <?=
          session()->get('Role') == 1
            ? '/admin'
            : (session()->get('Role') == 2
              ? '/hubin'
              : (session()->get('Role') == 3
                ? '/siswa'
                : ''));
        ?>" class="nav-link <?= ($methodName == 'Dashboard') ? 'active' : ''; ?>">
          <img src="<?= base_url('assets/image/icon-img/dashboard.svg'); ?>" width="25px" height="25px">
          <p class="pl-1">
            Dashboard
          </p>
        </a>
      </li>
      <!-- Dashboard end -->

      <!-- MENU TAMPILAN UNTUK ROLE 1 =  SUPER ADMIN AWAL -->
      <?php if (session()->get('Role') == 1) : ?>
        <!-- Dashboard start -->
        <li class="nav-item has-treeview">
          <!-- 2 condition => condition and condition ? ' ' : ' ' -->
          <a href="<?= route_to('users'); ?>" class="nav-link <?= ($methodName == 'users') ? 'active' : '' ?>">
            <img src="<?= base_url('assets/image/icon-img/user.svg'); ?>" width="28px" height="28px">
            <p class="pl-1">
              Akun Pengguna
            </p>
          </a>
        </li>
        <!-- Dashboard end -->
        <!-- menu start -->
        <li class="nav-item has-treeview <?= ($methodName != 'dataMaster') ? 'menu-close' : 'menu-open'; ?>">
          <a href="#" class="nav-link <?= ($methodName == 'dataMaster') ? 'active' : ''; ?>">
            <i class="nav-icon fas fa-compass"></i>
            <p>
              Data Master
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/admin/siswa" class="nav-link <?= $judul == 'Tabel Siswa' ? 'active' : ''; ?>">
                <i class="nav-icon fas fa-user-graduate"></i>
                <p>Siswa</p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="/admin/perusahaan" class="nav-link <?= $judul == 'Tabel Perusahaan' ? 'active' : ''; ?>">
                <i class="nav-icon fas fa-user-graduate"></i>
                <p>Perusahaan</p>
              </a>
            </li> -->
            <!-- <li class="nav-item">
              <a href="/admin/guru" class="nav-link">
                <i class="nav-icon fas fa-chalkboard-teacher"></i>
                <p>Pembimbing Guru</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-user-nurse"></i>
                <p>Pembimbing Perusahaan</p>
              </a>
            </li> -->
            <!-- </ul>
        </li> -->
            <!-- menu end -->
          <?php endif; ?>
          <!-- MENU TAMPILAN UNTUK ROLE 1 = SUPER ADMIN AKHIR-->

          <!-- MENU TAMPILAN UNTUK ROLE 2 = ADMIN HUBIN AWAL -->
          <?php if (session()->get('Role') == 2) : ?>
            <li class="nav-item has-treeview">
              <a href="<?= route_to('list_pengajuan_tmpt_prakerin'); ?>" class="nav-link <?= ($methodName == 'listpengajuan') ? 'active' : '' ?>">
                <img src="<?= base_url('assets/image/icon-img/list-pengajuan.svg'); ?>" width="25px" height="25px">
                <p class="pl-1">
                  List Pengajuan
                </p>
              </a>
            </li>

            <li class="nav-item has-treeview <?= ($methodName != 'perusahaan' and $methodName != 'dataMasterS') ? 'menu-close' : 'menu-open'; ?>">
              <a href="#" class="nav-link <?= ($methodName != 'dataMasterS' and $methodName != 'perusahaan') ? '' : 'active'; ?>">
                <img src="<?= base_url('assets/image/icon-img/data-master.svg'); ?>" width="25px" height="25px">
                <p class="pl-1">
                  Data Master
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?= route_to('v_siswa'); ?>" class="nav-link <?= ($methodName == 'dataMasterS') ? 'active' : '' ?>">
                    <img src="<?= base_url('assets/image/icon-img/siswa.svg'); ?>" width="25px" height="25px">
                    <p class="pl-1">
                      Siswa
                    </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= route_to('perusahaan_hubin'); ?>" class="nav-link <?= ($methodName == 'perusahaan') ? 'active' : '' ?>">
                    <img src="<?= base_url('assets/image/icon-img/perusahaan.svg'); ?>" width="25px" height="25px">
                    <p class="pl-1">
                      Perusahaan
                    </p>
                  </a>
                </li>


              </ul>
            </li>

          <?php endif; ?>
          <!-- MENU TAMPILAN UNTUK ROLE 2 = ADMIN HUBIN AKHIR -->

          <!-- MENU TAMPILAN UNTUK ROLE 3 = SISWA AWAL-->
          <?php if (session()->get('Role') == 3) : ?>
            <li class="nav-item has-treeview">
              <a href="<?= route_to('pengajuan'); ?>" class="nav-link <?= ($methodName == 'pengajuan') ? 'active' : '' ?>">
                <img src="<?= base_url('assets/image/icon-img/perusahaan.png'); ?>" width="25px" height="25px">
                <p class="pl-1">
                  Pengajuan
                </p>
              </a>
            </li>
          <?php endif; ?>
          <!-- MENU TAMPILAN UNTUK ROLE 3 = SISWA AWAL-->

          <!-- logout start -->
          <li class="nav-item has-treeview menu-close">
            <!-- 2 condition => condition and condition ? ' ' : ' ' -->
            <a href="<?= route_to('logout'); ?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
          <!-- logout end -->
          </ul>
  </nav>
</div>
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark"><?= $judul; ?></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="<?= (session()->get('Role') == 1
                          ? '/admin'
                          : (session()->get('Role') == 2))
                          ? '/hubin'
                          : (session()->get('Role') == 3
                            ? '/siswa'
                            : '');
                        ?>" class="text-decoration-none"><i class="fas fa-home"> Dashboard</i>
              </a>
            </li>
            <li class="breadcrumb-item active">
              <a class="text-text-decoration-none">
                <?php
                if ($methodName == 'Dashboard') {
                  echo '';
                } elseif ($methodName == 'users') {
                  echo '<i class="fas fa-users">  ' . $judul . '</i>';
                } elseif ($methodName == 'dataMasterS' and $judul == 'Tabel Siswa' or $judul == 'Tambah Data Siswa' or $judul == 'Edit Data Siswa') {
                  echo '<i class="fas fa-user-graduate">  ' . $judul . '</i>';
                } elseif ($methodName == 'dataMaster' and $judul == 'Tabel Master Pembimbing Guru') {
                  echo '<i class="fas fa-user-graduate">  ' . $judul . '</i>';
                } elseif ($methodName == 'dataMaster' and $judul == 'Tabel Perusahaan') {
                  echo '<i class="fas fa-user-graduate">  ' . $judul . '</i>';
                } elseif ($methodName == 'pengajuan') {
                  echo '<i class="fas fa-user-graduate">  ' . $judul . '</i>';
                } elseif ($methodName == 'profile') {
                  echo '<i class="fas fa-user-graduate">  ' . $judul . '</i>';
                } elseif ($methodName == 'listpengajuan') {
                  echo '<i class="fas fa-list-alt">  ' . $judul . '</i>';
                } elseif ($methodName == 'perusahaan') {
                  echo '<i class="fas fa-building">  ' . $judul . '</i>';
                } else {
                  echo 'hello icon dan judul belum di atur :)';
                }
                ?>
              </a>
            </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">