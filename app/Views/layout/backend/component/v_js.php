<!-- jQuery -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/jquery/jquery.min.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/template-dashboard-admin-lte/dist/js/adminlte.min.js'); ?>"></script>
<!-- sweet alert -->
<script src="<?= base_url('assets/js/sweetalert2.all.min.js'); ?>"></script>
<!-- Custom JS -->
<script src="<?= base_url('assets/js/custom.js') ?>"></script>
</body>

</html>