<!--================== Hallo bro ... ===================================================================================
     ================== Nyari apa nih hehe .... =========================================================================
      ================== Ngintip-ngintip aja nih ... =====================================================================
       ================== HUSST JANGAN BILANG BILANG UDAH LIAT INI !!! ====================================================

      ░█████╗░██████╗░██████╗░██╗  ███╗░░██╗██╗░░░██╗██████╗░  ██╗███╗░░██╗░██████╗░█████╗░███╗░░██╗
      ██╔══██╗██╔══██╗██╔══██╗██║  ████╗░██║██║░░░██║██╔══██╗  ██║████╗░██║██╔════╝██╔══██╗████╗░██║
      ███████║██████╔╝██║░░██║██║  ██╔██╗██║██║░░░██║██████╔╝  ██║██╔██╗██║╚█████╗░███████║██╔██╗██║
      ██╔══██║██╔══██╗██║░░██║██║  ██║╚████║██║░░░██║██╔══██╗  ██║██║╚████║░╚═══██╗██╔══██║██║╚████║
      ██║░░██║██║░░██║██████╔╝██║  ██║░╚███║╚██████╔╝██║░░██║  ██║██║░╚███║██████╔╝██║░░██║██║░╚███║
      ╚═╝░░╚═╝╚═╝░░╚═╝╚═════╝░╚═╝  ╚═╝░░╚══╝░╚═════╝░╚═╝░░╚═╝  ╚═╝╚═╝░░╚══╝╚═════╝░╚═╝░░╚═╝╚═╝░░╚══╝
      
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
<!doctype html>
<html lang="en" id="home">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- title -->
  <title><?= $title; ?></title>

  <!-- css font-awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

  <!-- font google -->
  <link href="https://fonts.googleapis.com/css2?family=Comic+Neue:ital,wght@0,700;1,400;1,700&family=Poppins:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600&family=Viga&display=swap" rel="stylesheet">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

  <?= ($title != 'Login' and $title != 'Register Akun') ? '' : '
    <!-- css custom -->
    <link rel="stylesheet" href="assets/template-dashboard-admin-lte/css/adminlte.min.css">';
  ?>

  <!-- css custom -->
  <link rel="stylesheet" href="<?= base_url('assets/css/auth.css'); ?>">

</head>

<body class="background-img">
  <?= $this->renderSection('content'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="<?= base_url('assets/bootstrap/js/bootstrap.bundle.min.js'); ?>" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js'); ?>" crossorigin="anonymous"></script>
  <!-- sweet alert -->
  <script src="<?= base_url('assets/js/sweetalert2.all.min.js'); ?>"></script>
  <!-- custom js -->
  <script src="<?= base_url('assets/js/custom.js'); ?>"></script>
</body>

</html>