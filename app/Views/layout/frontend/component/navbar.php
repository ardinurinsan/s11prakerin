<header id="head-section">
  <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-primary" id="nav-scroll">
    <div class="container">
      <a class="navbar-brand judul" href="#home">S11PrakerinKuy</a>
      <button class="navbar-toggler collapsed btn-border-light" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="#home">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#about">ABOUT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#opening-learn">JURUSAN</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#contact-menu">CONTACT</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">PERUSAHAAN</a></a>
          </li>
          <?= (session()->get('login') == true) ?
            '
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user">   ' . session()->get('Username') . '</i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="' .
            (session()->get('Role') == 1 ? '/admin' : (session()->get('Role') == 2 ? '/hubin' : (session()->get('Role') == 3 ? '/siswa' : ''))) . '">Dashboard</a>
                  <a class="dropdown-item" href="' . (session()->get('Role') == 3 ? '/siswa/profile' : "") . '">Profile</a>
                  <a class="dropdown-item" href="#">Help</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="' . route_to('logout') . '">Logout</a>
                </div>
              </li>
            '
            :
            '<li class="nav-item"><a class="nav-link rounded-pill px-4 text-white text-mobile-white btn-sm btn-outline-light btn-primary bg-light  ml-3 btn-login shadow-sm" href="' . route_to('login') . '"><i class="fas fa-sign-in-alt"></i> LOGIN</a> </li>';
          ?>
        </ul>
      </div>
    </div>
  </nav>
</header>
<div data-spy="scroll" data-target="nav-scroll" data-offset="0">