<?= $this->extend('layout/frontend/template'); ?>

<?= $this->section('content'); ?>
<?= $this->include('layout/frontend/component/navbar'); ?>
<div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>

<main class="content">
  <section id="hero-section">
    <div class="container mt-3">
      <div class="row justify-content-between align-items-center">
        <div class="col-md-6 text-center text-md-left mb-5">
          <h1 class="display-4"><span id="typed"></span></h1>
          <p class="judul-hero lead lead text-secondary">Sekolah Menengah Kejuruan Negeri 11 Kota Bandung.</p>
          <a href="/login" class="btn btn-primary rounded-pill px-4 mt-2" role="button">Login</a>
        </div>
        <div class="col-md-6">
          <img src="<?= base_Url('assets/image/logo-img/hero.svg'); ?>" class="img-hero" alt="">
        </div>
      </div>
    </div>
    <div class="ombak">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path fill="#3384FE" fill-opacity="1" d="M0,128L48,149.3C96,171,192,213,288,213.3C384,213,480,171,576,176C672,181,768,235,864,261.3C960,288,1056,288,1152,245.3C1248,203,1344,117,1392,74.7L1440,32L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path>
      </svg>
    </div>
  </section>

  <section id="about-section">
    <div class="container" id="about">
      <h2 class="section-title judul-about">About</h2>
      <span class="section-line"></span>
      <div class="row justify-content-between align-items-center my-5 text-center text-md-left">
        <div class="col-md-6 col-lg-5">
          <div class="feature-img">
            <img src="<?= base_url('assets/Image/bg-img/bg-body.png'); ?>" class="img-fluid" alt="">
          </div>
        </div>
        <div class="col-md-6 col-lg-6">
          <div class="feature-detail mt-3">
            <p class="h3 font-weight-bold">S11Prakerin Kuy</p>
            <span class="feature-line"></span>
            <p class="text-secondary text-justify">
              Adalah sebuah website mengenai Prakerin atau Praktek Kerja Industri, yang dapat membantu guru dalam pemetaan siswa Prakerin
            </p>
            <a href="#" class="btn btn-outline-primary px-4 py-2 font-weight-bold rounded-pill">Lihat kelebihan</a>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-between align-items-center my-5 flex-md-row-reverse text-center text-md-left">
        <div class="col-md-6 col-lg-5">
          <div class="feature-img">
            <img src="<?= base_url('assets/Image/bg-img/bg-drone1.jpg'); ?>" class="img-fluid" alt="">
          </div>
        </div>
        <div class="col-md-6 col-lg-6">
          <div class="feature-detail mt-3">
            <p class="h3 font-weight-bold">Kelebihan aplikasi S11Prakerin Kuy</p>
            <span class="feature-line"></span>
            <p class="text-secondary text-justify">
              <ul>
                <li>Menghemat waktu dalam pendataan siswa prakerin</li>
                <li>Memudahkan guru dalam pendataan</li>
                <li>Memudahkan siswa dalam mendapatkan info prakerin</li>
              </ul>
            </p>
            <!-- <a href="#" class="btn btn-outline-primary px-4 py-2 font-weight-bold rounded-pill">Lihat </a> -->
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- opening learn start -->
  <section id="opening-learn">
    <div class="container mb-5 mt-5">
      <h1 class="text-center judul-jurusan">Jurusan</h1>
      <hr>
      <div class="row mt-5">
        <div class="col-md-3">
          <div class="card box-learn">
            <img src="<?= base_url('assets/image/logo-img/RPL.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">RPL</h3>
              <p class="card-text">Rekayasa Perangkat Lunak.</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card box-learn  box-warna">
            <img src="<?= base_url('assets/image/logo-img/MM.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">MM</h3>
              <p class="card-text">MultiMedia</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card box-learn  box-warna">
            <img src="<?= base_url('assets/image/logo-img/TKJ.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">TKJ</h3>
              <p class="card-text">Teknik Komputer Dan Jaringan</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card box-learn">
            <img src="<?= base_url('assets/image/logo-img/OTKP.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">OTKP</h3>
              <p class="card-text">Otomatisasi dan Tata kelola Perkantoran</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-2 justify-content-center">
        <div class="col-md-3">
          <div class="card box-learn box-warna">
            <img src="<?= base_url('assets/image/logo-img/BDP.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">BDP</h3>
              <p class="card-text">Bisnis Daring Dan Pemasaran</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card box-learn">
            <img src="<?= base_url('assets/image/logo-img/MLOG.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">MLOG</h3>
              <p class="card-text">Management Logistik.</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="card box-learn box-warna">
            <img src="<?= base_url('assets/image/logo-img/AKL.jpeg'); ?>" class="" draggable="false" alt="...">
            <div class="card-body">
              <h3 class="card-title">AKL</h3>
              <p class="card-text">Akuntansi Dan Lembaga Keuangan</p>
            </div>
            <div class="card-footer footer-warna">
              <small class="text-dark">ardinur_03</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- opening learn end -->

  <!--contact section start-->
  <section id="contact-menu">
    <h2 class="text-center judul-kontak mt-5 text-white">Contact</h2>
    <div class="contact-section">
      <div class="contact-info">
        <div><i class="fas fa-map-marker-alt"></i> Jl. Budhi Cilember, Bandung, Jawa Barat, Indonesia</div>
        <div><i class="fas fa-envelope"></i> smkn11bdg@gmail.com</div>
        <div><i class="fas fa-phone"></i>022-6652442 (telp)
          022-6613508 (faks)</div>
      </div>
      <!-- <div class="contact-form"> -->
      <!-- <--<form class="contact" action="" method="post">
        <input type="text" name="name" class="text-box" placeholder="Your Name" required>
        <input type="email" name="email" class="text-box" placeholder="Your Email" required>
        <textarea name="message" rows="5" placeholder="Your Message" required></textarea>
        <input type="submit" name="submit" class="send-btn" value="Send">
        </form> -->
    </div>
    </div>
  </section>
  <!--contact section end-->

</main>

<!-- start section footer -->
<footer id="footer" class="text-center">
  <div class="container">
    <div class="socials-media text-center">
      <ul class="list-unstyled">
        <li><a href="https://web.facebook.com/ardinurinsan03" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
        <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fab fa-twitter"></i></a></li>
        <li><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fab fa-instagram"></i></a></li>
        <li><a href="#" target="_blank" data-toggle="tooltip" data-placement="top" title="YouTube"><i class="fab fa-youtube"></i></a></li>
      </ul>
    </div>
    <p>&copy; Copyright <script>
        document.write(new Date().getFullYear());
      </script> <i class="fas fa-heart"></i></p>
    <div class="credits" style="color: white;">
      Designed by <a href="https://www.ardinur.space" target="_blank">ardinur_03</a>
    </div>
  </div>
</footer>
<!-- End section footer -->
<?= $this->endsection(); ?>