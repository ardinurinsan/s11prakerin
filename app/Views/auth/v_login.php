<?= $this->extend('layout/frontend/template_login'); ?>
<?= $this->section('content'); ?>
<section id="section-login">
  <div class="container mt-5">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-6 col-xl-4">
        <div class="card shadow-lg">
          <div class="card-body bg-light login-card-body">
            <div class="row text-center">
              <div class="col">
                <img src="<?= base_url('assets/image/logo-img/smk11.jpg'); ?>" width="100px" alt="">
              </div>
            </div>

            <h3 class="login-box-msg">Login</h3>
            <?php if (!empty(session()->getFlashdata('validate'))) {
            ?>
              <div class="alert alert-danger">
                <?= $validate->listErrors(); ?>
              </div>
            <?php } ?>

            <!-- sweetAlert -->
            <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>
            <div class="ggl" data-ggl="<?= session()->get('message_error'); ?>"></div>
            <div class="info" data-info="<?= session()->get('message_info'); ?>"></div>

            <?php
            echo form_open('proses_login');
            csrf_field();
            ?>
            <div class="input-group mb-3">
              <input type="email" name="Email" value="<?= old('Email'); ?>" class="form-control" placeholder="Email">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" name="Password" value="<?= old('Password'); ?>" class="form-control" placeholder="Password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <a href="<?= route_to('register'); ?>" class="text-center">Tidak punya akun?</a>
              </div>

              <!-- <div class="col-8">
                    <div class="icheck-primary">
                      <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                      <label for="agreeTerms">
                        I agree to the <a href="#">terms</a>
                      </label>
                    </div>
                  </div> -->
              <div class="col">
                <button type="submit" class="btn btn-primary btn-sm ml-5"><i class="fa fa-sign-in-alt"></i> Login</button>
              </div>
            </div>
            <?= form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->endSection(); ?>