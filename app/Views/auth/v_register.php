<?= $this->extend('layout/frontend/template'); ?>
<?= $this->section('content'); ?>
<section id="section-login">
  <div class="container mt-5">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-6 col-xl-auto">


        <div class="card shadow-lg">
          <div class="card-body register-card-body">
            <h3 class="login-box-msg">Daftar akun baru</h3>
            <?php if (!empty(session()->getFlashdata('validate'))) {
            ?>
              <div class="alert alert-danger">
                <?= $validate->listErrors(); ?>
              </div>
            <?php } ?>

            <?php
            echo form_open('proses_register');
            csrf_field();
            ?>
            <div class="input-group mb-3">
              <input type="text" name="Username" value="<?= old('Username'); ?>" class="form-control" placeholder="Full name">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="email" name="Email" value="<?= old('Email'); ?>" class="form-control" placeholder="Email">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" name="Password" value="<?= old('Password'); ?>" class="form-control" placeholder="Password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" name="Repassword" class="form-control" placeholder="Retype password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <div class="input-group-append">
                <div class="input-group-text">
                  Level
                </div>
              </div>
              <select class="form-control" name="Role" id="Role">
                <option value=" ">No selected</option>
                <?php foreach ($role as $r) : ?>
                  <option value="<?= $r['Id_Role'] ?>"><?= $r['Nama_Role'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="row">
              <div class="col-7">
                <a href="<?= route_to('login'); ?>" class="text-center">Sudah punya akun?</a>
              </div>

              <!-- <div class="col-8">
                    <div class="icheck-primary">
                      <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                      <label for="agreeTerms">
                        I agree to the <a href="#">terms</a>
                      </label>
                    </div>
                  </div> -->
              <div class="col-5">
                <button type="submit" class="btn btn-success btn-sm"> <i class="fa fa-sign-in-alt"></i> Register</button>
              </div>
            </div>
            <?= form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> <?= $this->endSection(); ?>