<section class="content">
  <div class="error-page">
    <div class="container">
      <div class="row">
        <div class="col">
          <img src="<?= base_url('assets/image/bg-img/maintenance.svg'); ?>" alt="" width="100%" height="350px">
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Fitur ini dalam masa Maintenance :).</h3>

          <p>
            Kembali ke dashboard?.
            <a href="/siswa" class="text-decoration-none">Klik saya !!!</a>.
          </p>
        </div>
      </div>
    </div>
  </div>

</section>