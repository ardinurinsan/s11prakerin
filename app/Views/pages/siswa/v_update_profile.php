<!-- Setting Profile start -->
<div class="tab-pane active" id="Setting-Profile">
  <form action="<?= base_url('siswa'); ?>" method="POST">

    <div class="form-group row">
      <label for="namasis" class="col-sm-2 col-form-label">Nama</label>
      <div class="col-sm-10">
        <input type="teext" class="form-control" id="namasis" name="Nama_Siswa" value="<?= session()->get('siswa')->Nama_Siswa; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" name="Email" id="inputEmail" value="<?= session()->get('siswa')->Email; ?>" readonly>
      </div>
    </div>
    <div class=" form-group row">
      <label for="alamatsis" class="col-sm-2 col-form-label">Alamat</label>
      <div class="col-sm-10">
        <textarea class="form-control" name="Alamat" id="quoalamatsiste"><?= session()->get('siswa')->Alamat; ?></textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="quote" class="col-sm-2 col-form-label">Quote</label>
      <div class="col-sm-10">
        <textarea class="form-control" name="Quote_Siswa" id="quote"><?= session()->get('siswa')->Quote_Siswa; ?></textarea>
      </div>
    </div>
    <div class=" form-group row">
      <div class="offset-sm-2 col-sm-10 mt-3">
        <button type="submit" class="btn btn-outline-info-icon btn-info btn-sm"><i class="far fa-edit"></i> Simpan Perubahan</button>
      </div>
    </div>
  </form>
</div>
<!-- setting profile end-->