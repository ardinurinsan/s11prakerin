<section class="profile-siswa">
  <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">

        <!-- Profile Image -->
        <div class="card">
          <div class="card-header text-center">
            Profile
          </div>
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dataUsers/img/' . session()->get('Foto')); ?>" alt="User profile picture">
            </div>

            <h3 class="profile-username text-center"><?= session()->get('siswa')->Nama_Siswa ?></h3>

            <p class="text-muted text-center"><?= session()->get('siswa')->Nama_Jurusan ?></p>

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>NIS</b> <a class="float-right"><?= session()->get('siswa')->NIS ?></a>
              </li>
              <li class="list-group-item">
                <b>Kelas</b> <a class="float-right"><?= session()->get('siswa')->Kelas ?></a>
              </li>
              <li class="list-group-item">
                <b>Jenis Kelamin</b> <a class="float-right"><?php
                                                            $jk = session()->get('siswa')->JK_Siswa;
                                                            if ($jk == "L") {
                                                              echo 'Laki Laki';
                                                            } else {
                                                              echo 'Perempuan';
                                                            }
                                                            ?>
                </a>
              </li>
              <li class="list-group-item">
                <b>No HP</b> <a class="float-right"><?= session()->get('siswa')->Kontak_Siswa ?></a>
              </li>
              <li class="list-group-item">
                <b>Email</b> <a class="float-right"><?= session()->get('siswa')->Email ?></a>
              </li>
              <li class="list-group-item">
                <b>Alamat</b> <a class="float-right"><?= session()->get('siswa')->Alamat ?></a>
              </li>
              <li class="list-group-item text-center">
                <b>My Quotes</b> <br>
                <blockquote class="blockquote text-center">
                  <p><?= $siswa->Quote_Siswa; ?></p>
                  <footer class="blockquote-footer"><?= session()->get('siswa')->Username ?></footer>
                </blockquote>
              </li>
            </ul>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->

      <div class="col-md-8">
        <div class="card">
          <div class="card-header p-2">
            <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link <?= (!$validation->hasError('Password')) ? 'active' : ''; ?>" href="#sekolahku" data-toggle="tab">Sekolahku</a></li>
              <li class="nav-item"><a class="nav-link <?= ($validation->hasError('Password')) ? 'active' : ''; ?>" href="#repw" data-toggle="tab">Reset Password</a></li>
              <li class="nav-item"><a class="nav-link" href="#quote" data-toggle="tab">Quotes</a></li>
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content">
              <div class="tab-pane <?= (!$validation->hasError('Password')) ? 'active' : ''; ?>  m-3" id="sekolahku">

                <div class="row">
                  <div class="col-3">
                    <img src="<?= base_url('assets/image/logo-img/smk11.jpg'); ?>" height="180px" alt="">
                  </div>
                  <div class="col-7 ml-4">
                    <strong><i class="fas fa-school mr-1"></i> Sekolah</strong>
                    <p class="text-muted">
                      Sekolah Menengah Kejuruan Negeri 11 Kota Bandung
                    </p>
                    <hr>
                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi</strong>
                    <p class="text-muted">Jalan Budi Sukaraja, Kec. Cicendo, Kota Bandung, Jawa Barat 40153</p>
                    <hr>
                  </div>
                </div>
              </div>

              <div class="tab-pane <?= ($validation->hasError('Password')) ? 'active' : ''; ?>" id="repw">
                <form class="form-horizontal" action="<?= base_url('siswa/profile/edit/' . $siswa->Id_User); ?>" method="POST">
                  <?= csrf_field(); ?>
                  <input type="hidden" name="Id_User" value="<?= $siswa->Id_User; ?>">
                  <div class="form-group row">
                    <label for="passnew" class="col-sm-3 col-form-label">Password Baru</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control <?= ($validation->hasError('Password')) ? 'is-invalid' : ''; ?>" value="<?= old('Password'); ?>" name="Password" id="passnew" placeholder="Password baru">
                      <div class="invalid-feedback">
                        *<?= $validation->getError('Password'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="repass" class="col-sm-3 col-form-label">Ketik Ulang Password Baru</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control <?= ($validation->hasError('Repassword')) ? 'is-invalid' : ''; ?>" value="<?= old('Repassword'); ?>" name="Repassword" id="repass" placeholder="Ketik ulang password baru">
                      <div class="invalid-feedback">
                        *<?= $validation->getError('Repassword'); ?>
                      </div>
                    </div>
                  </div>
                  <div class="row justify-content-end mr-3">
                    <div class="">
                      <button type="submit" class="btn btn-outline-accent-success btn-success"><i class="fas fa-save"></i> Submit</button>
                    </div>
                  </div>
                </form>

              </div>

              <div class="tab-pane" id="quote">
                <form class="form-horizontal" action="<?= base_url('siswa/profile/edit-quote/' . $siswa->NIS); ?>" method="POST">
                  <?= csrf_field(); ?>
                  <input type="hidden" name="NIS" value="<?= $siswa->NIS; ?>">
                  <div class="form-group row">
                    <label for="quotesis" class="col-sm-3 col-form-label">Quotes</label>
                    <div class="col-sm-9">
                      <textarea name="Quote_Siswa" id="quotesis" class="form-control" rows="5" required><?= $siswa->Quote_Siswa; ?></textarea>
                    </div>
                  </div>
                  <div class="row justify-content-end mr-3">
                    <div class="">
                      <button type="submit" class="btn btn-outline-accent-success btn-success"><i class="fas fa-save"></i> Submit</button>
                    </div>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>