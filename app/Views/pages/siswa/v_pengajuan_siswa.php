<section id="jumbotron-pengajuan">
  <!-- Sweetalert start -->
  <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>
  <div class="ggl" data-ggl="<?= session()->get('message_errors'); ?>"></div>
  <!-- Sweetalert end -->

  <?php
  $no = 0;
  if ($siswa->NIS != null) :
  ?>
    <?php foreach ($pengajuan as $p) { ?>
      <div class="card">
        <h5 class="card-header <?= ($pengajuan[$no]['Status_Pengajuan'] == 0) ? 'bg-warning' : ($pengajuan[$no]['Status_Pengajuan'] == 1 ? 'bg-success' : ($pengajuan[$no]['Status_Pengajuan'] == 2 ? 'bg-danger' : '')) ?>"><?= ($pengajuan[$no]['Status_Pengajuan'] == 0) ? 'Formulir Pengajuan anda dalam proses pemeriksaan Admin :)' : ($pengajuan[$no]['Status_Pengajuan'] == 1 ? 'Selamat pengajuan anda Diterima !!!' : ($pengajuan[$no]['Status_Pengajuan'] == 2 ? 'Pengajuan anda di Tolak !!! anda bisa mengajukan tempat prakerin lagi' : '')) ?></h5>
        <div class="card-body">
          <!-- Cek status -->
          <?php
          $ajuan = $pengajuan[$no]['Status_Pengajuan'];
          if ($ajuan == 0) {
            $ribbonColor = 'bg-warning';
          } elseif ($ajuan == 1) {
            $ribbonColor = 'bg-success';
          } elseif ($ajuan == 2) {
            $ribbonColor = 'bg-danger';
          }
          ?>
          <div class="ribbon-wrapper ribbon-xl">
            <div class="ribbon text-md <?= $ribbonColor; ?>">
              <?=
                ($ajuan == 0) ? 'Proses Pemeriksaan'
                  : ($ajuan == 1 ? 'Proses Pengajuan Diterima' : ($ajuan == 2
                    ? 'Pengajuan Di Tolak' : ''));
              ?>
            </div>
          </div>
          <div class="container-fluid">
            <div class="row">
              <div class="col">
                <table>
                  <h5 class="text-muted">* Data Perusahaan yang di ajukan</h5>
                  <hr class="hr">
                  <tr>
                    <td>Nama Perusahaan</td>
                    <td>:</td>
                    <td id="nama_perusahaan"> <?= $pengajuan[$no]['Nama_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Bidang Perusahaan</td>
                    <td>:</td>
                    <td id="nama_bidang_perus"><?= $pengajuan[$no]['Bidang_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Nama Pimpinan Perusahaan</td>
                    <td>:</td>
                    <td id="nama_pimpinan_perus"><?= $pengajuan[$no]['Nama_Pimpinan_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Jabatan Pimpinan</td>
                    <td>:</td>
                    <td id="jabatan"><?= $pengajuan[$no]['Jabatan'] ?></td>
                  </tr>
                  <tr>
                    <td>Email Perusahaan</td>
                    <td>:</td>
                    <td id="emailperus"><?= $pengajuan[$no]['Email_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Kontak Perusahaan</td>
                    <td>:</td>
                    <td id="kontak_perusahaan"><?= $pengajuan[$no]['Kontak_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Alamat Perusahaan</td>
                    <td>:</td>
                    <td id="alamat"><?= $pengajuan[$no]['Alamat_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Website Perusahaan</td>
                    <td>:</td>
                    <td id="website"><?= $pengajuan[$no]['Website_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Kota Perusahaan</td>
                    <td>:</td>
                    <td id="kota"><?= $pengajuan[$no]['Kota_Perusahaan'] ?></td>
                  </tr>
                  <tr>
                    <td>Kode Pos Perusahaan</td>
                    <td>:</td>
                    <td id="kodepos"><?= $pengajuan[$no]['Kode_Pos'] ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Pengajuan Awal</td>
                    <td>:</td>
                    <td id="tglawal"><?= $pengajuan[$no]['Tglbulan_Awal'] ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Pengajuan Akhir</td>
                    <td>:</td>
                    <td id="tglakhir"><?= $pengajuan[$no]['Tglbulan_Akhir'] ?></td>
                  </tr>
                </table>
              </div>
              <div class="col-7">
                <?php
                if ($ajuan == 0) {
                  $namaGambar = 'img-camping.svg';
                } elseif ($ajuan == 1) {
                  $namaGambar = 'img-happy.svg';
                } elseif ($ajuan == 2) {
                  $namaGambar = 'img-sad.svg';
                }

                ?>
                <img src="<?= base_url('assets/Image/logo-img/' . $namaGambar . ''); ?>" width="100%" height="auto" alt="">
              </div>
            </div>
          </div>
          <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
        </div>
      </div>


    <?php $no++;
    }; ?>

  <?php endif; ?>

  <?php if ($siswa->Jumlah_Pengajuan != 0) : ?>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7">
          <img src="<?= base_url('assets/image/logo-img/perusahaan.svg'); ?>" class="img-fluid" alt="">
        </div>
        <div class="col-md-12">
          <div class="jumbotron-fluid shadow-md bg-accent-light">
            <h1 class="display-4">Pengajuan Tempat Prakerin</h1>
            <p class="lead">Anda hanya bisa mengajukan 1 kali pengajuan, kecuali jika pengajuan anda di tolak maka anda bisa mengajukan kembali.</p>
            <hr class="my-4">
            <p>klik tombol di bawah ini untuk mengajukan</p>
            <button data-toggle="modal" data-target="#btn-pengajuan" class="btn btn-warning btn-md text-white btn-outline-alert-warning border-0" href="#" role="button"> <i class="fab fa-telegram-plane"></i> &nbsp;Ajukan sekarang ?</button>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <!-- Modal -->
  <div class="modal fade" id="btn-pengajuan" tabindex="-1" aria-labelledby="pengajuanprakerin" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="pengajuanprakerin">Pengajuan Tempat Perakerin</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php
          echo form_open('siswa/pengajuan/simpan');
          helper('text');
          $id_sp = random_string('alnum', 5);
          $nis = session()->get('siswa')->NIS;
          ?>

          <div class="form-row">
            <input type="hidden" name="Id_SP" value="<?= $id_sp; ?>">
            <input type="hidden" name="NIS" value="<?= $nis; ?>">
            <div class="form-group col-md-6">
              <label for="namaperus">Nama Perusahaan</label>
              <input type="text" class="form-control" id="namaperus" name="Nama_Perusahaan" required>
            </div>

            <div class="form-group col-md-6">
              <label for="JenisPerus">Bidang Perusahaan</label>
              <input type="text" class="form-control" id="JenisPerus" name="Bidang_Perusahaan" required>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="namapem">Nama Pemimpin Perusahaan</label>
              <input type="text" class="form-control" id="namapem" name="Nama_Pimpinan_Perusahaan" required>
            </div>

            <div class="form-group col-md-6">
              <label for="jabatan">Jabatan</label>
              <input type="text" class="form-control" id="jabatan" name="Jabatan" required>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="notelepon">No Telepon Perusahaan</label>
              <input type="number" class="form-control" id="notelepon" name="Kontak_Perusahaan" required>
            </div>

            <div class="form-group col-md-4">
              <label for="emailperus">Email Perusahaan</label>
              <input type="text" class="form-control" id="emailperus" name="Email_Perusahaan" required>
            </div>

            <div class="form-group col-md-4">
              <label for="websiteperus">Website Perusahaan (opsional)</label>
              <input type="text" class="form-control" id="websiteperus" name="Website_Perusahaan" required>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="kota">Kota (opsional)</label>
              <input type="text" class="form-control" name="Kota_Perusahaan" id="kota">
            </div>
            <div class="form-group col-md-4">
              <label for="kodepos">Kode Pos (opsional)</label>
              <input type="text" class="form-control" name="Kode_Pos" id="kodepos">
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="blnawal">Bulan Awal</label>
              <input type="date" class="form-control" name="Tglbulan_Awal" id="blnawal">
              <small class="text-red">*Tanggal ini merupakan tanggal untuk pengajuan pelaksanaan prakerin</small>
            </div>
            <div class="form-group col-md-6">
              <label for="blnakhir">Sampai dengan bulan</label>
              <input type="date" class="form-control" name="Tglbulan_Akhir" id="blnakhir">
            </div>
          </div>

          <div class="form-group">
            <label for="inputAddress">Alamat</label>
            <textarea type="text" class="form-control" id="inputAddress" name="Alamat_Perusahaan" placeholder="Alamat ..."></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fas fa-arrow-left"></i> Tutup</button>
          <button type="reset" class="btn btn-info btn-sm"><i class="fas fa-trash-restore"></i> Reset</button>
          <button type="submit" class="btn btn-success btn-sm"><i class="fab fa-telegram-plane"></i> Ajukan</button>
        </div>
        <?= form_close(); ?>
      </div>
    </div>
  </div>
</section>