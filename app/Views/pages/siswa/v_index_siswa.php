<?php
$no = 0;
$statajuan = $siswa->Jumlah_Pengajuan;
?>
<section id="dashboard-siswa">
  <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>

  <div class="jumbotron-fluid">

    <div class="row">
      <div class="col">
        <h2>Selamat datang <?= session()->get('siswa')->Nama_Siswa; ?></h2>
        <p class="lead"></p>
        <hr class="my-4">


      </div>
    </div>
    <div class="row justify-content-end">
      <div class="col">
        <img src="<?= base_url('assets/image/logo-img/img-camping.svg') ?>" height="450px" alt="">
      </div>
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?= $jmlh_perusahaan; ?></h3>

            <p>Perusahaan</p>
          </div>
          <div class="icon">
            <i class="far fa-building"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>