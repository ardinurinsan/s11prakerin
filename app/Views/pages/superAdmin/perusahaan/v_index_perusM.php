<section id="section-perusahaan">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Perusahaan yang berkerja sama dengan SMK Negeri 11 Bandung</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <table class="table table-responsive table-sm" id="tabeldata">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Perusahaan</th>
                  <th>Nama Pimpinan Perusahaan</th>
                  <th>Jabatan</th>
                  <th>Alamat Perusahaan</th>
                  <th>Email</th>
                  <th>Website</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($perusahaan as $p => $value) :
                ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $value['Nama_Perusahaan']; ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                      <div class="btn-group">
                        <div class="btn-group d-inline-flex">
                          <a href="" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-sm text-white"><i class="fas fa-search"></i></a>
                          <button data-toggle="modal" data-tooltip="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-sm btn-default"><i class="far fa-trash-alt"></i></button>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script>
  $(document).ready(function() {
    $('#tabeldata').DataTable({
      dom: 'Blfrtip',
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
      buttons: [
        'copy',
        'excel',
        'print'
      ]
    });
  });
</script>