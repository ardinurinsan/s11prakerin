<section id="table-siswa">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Data Siswa</h3>

            <div class="card-tools">
              <a href="<?= route_to('create_siswa'); ?>" class="btn btn-success btn-sm"><i class="fas fa-user-graduate"></i> <i class="fas fa-plus"></i> </a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
            <!-- /.card-tools -->
          </div>
          <!-- /.card-header -->

          <!-- sweetalert start -->
          <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>
          <div class="ggl" data-ggl="<?= session()->get('message_errors'); ?>"></div>
          <!-- sweetalert end -->

          <div class="card-body">
            <div class="container">
              <table id="tabeldata" class="table table-responsive-md table-sm table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama Siswa</th>
                    <th>Kelas</th>
                    <th>Jenis Kelamin</th>
                    <th>Jurusan</th>
                    <th>kontak</th>
                    <th>Alamat</th>
                    <th>Banyak Pengajuan</th>
                    <th scope="colgroup">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($siswa as $s => $value) {
                  ?>
                    <tr>
                      <td><?= $no++; ?></td>
                      <td><?= $value['NIS']; ?></td>
                      <td><?= $value['Nama_Siswa']; ?></td>
                      <td><?= $value['Kelas']; ?></td>
                      <td><?= $value['JK_Siswa']; ?></td>
                      <td><?= $value['Nama_Jurusan']; ?></td>
                      <td><?= $value['Kontak_Siswa']; ?></td>
                      <td><?= $value['Alamat']; ?></td>
                      <td><?= $value['Jumlah_Pengajuan']; ?></td>
                      <td>
                        <div class="btn-group d-inline-flex">
                          <a href="<?= base_url('/admin/siswa/edit/' . $value['NIS']) ?>" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                          <button data-toggle="modal" id="btn-detail-siswa" data-target="#detail" data-tooltip="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-sm text-white" data-nis="<?= $value['NIS']; ?>" data-namasis="<?= $value['Nama_Siswa']; ?>" data-kelas="<?= $value['Kelas']; ?>" data-jk="<?= $value['JK_Siswa']; ?>" data-alamat="<?= $value['Alamat']; ?>" data-kontaksis="<?= $value['Kontak_Siswa']; ?>" data-jurusan="<?= $value['Nama_Jurusan']; ?>"><i class="fas fa-search"></i></button>
                          <button data-toggle="modal" data-target="#hapusdata<?= $value['NIS']; ?>" data-tooltip="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-sm btn-default"><i class="far fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!-- /.card -->
    </div>

    <!-- Modal -->
    <?php foreach ($siswa as $key => $value) { ?>
      <div class="modal fade" id="hapusdata<?= $value['NIS']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body justify-content-center">
              <h5>Apakah anda yakin untuk menghapus data ini?</h5>
            </div>

            <div class="modal-footer">
              <form name="formHapus" data-id="<?= $value['NIS']; ?>" action="<?= base_url('/admin/siswa/' . $value['NIS'] . ''); ?>" method="POST">
                <?= csrf_field(); ?>
                <input type="hidden" name="_method" value="DELETE">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-danger btn-sm">Ya hapus !!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  <!-- Button trigger modal -->
  <!-- <input type="text" id="nis">
          <input type="text" id="namasis">
          <input type="text" id="kelas">
          <input type="text" id="jk">
          <input type="text" id="alamat">
          <input type="text" id="kontaksis">
          <input type="text" id="jurusan">
          <h1 id="jurusana"></h1> -->
  <!-- Modal -->
  <div class="modal fade" id="detail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="card">
          <h5 class="card-header bg-light">Detail Siswa dengan NIS <span id="nis"></span></h5>
          <div class="card-body">
            <table>
              <tr>
                <th>Nama Siswa</th>
                <td>:</td>
                <td><span id="namasis"></span></td>
              </tr>
              <tr>
                <th>Kelas</th>
                <td>:</td>
                <td><span id="kelas"></span></td>
              </tr>
              <tr>
                <th>Jenis Kelamin</th>
                <td>:</td>
                <td><span id="jk"></span></td>
              </tr>
              <tr>
                <th>Alamat</th>
                <td>:</td>
                <td><span id="alamat"></span></td>
              </tr>
              <tr>
                <th>Nomor HP</th>
                <td>:</td>
                <td><span id="kontaksis"></span></td>
              </tr>
              <tr>
                <th>Jurusan</th>
                <td>:</td>
                <td><span id="jurusan"></span></td>
              </tr>
            </table>
          </div>

        </div>
        <div class="modal-footer bg-light">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {
    $('#tabeldata').DataTable({
      dom: 'Blfrtip',
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
      buttons: [
        'copy',
        'excel',
        'print'
      ]
    });
  });
</script>