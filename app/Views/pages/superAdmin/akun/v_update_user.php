<section class="update-user">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Users</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
            <!-- /.card-tools -->
          </div>
          <!-- /.card-header -->
          <div class="card-body" style="display: block;">
            <?= form_open_multipart('admin/users/process_update/' . $users->Id_User); ?>
            <?= csrf_field(); ?>
            <input type="hidden" name="FotoLama" value="<?= $users->Foto; ?>">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="username">Username</label>
                <input type="text" name="Username" class="form-control <?= ($validation->hasError('Username')) ? 'is-invalid' : ''; ?>" id="username" value="<?= $users->Username; ?>" autocomplete>
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Username'); ?>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" name="Email" class="form-control <?= ($validation->hasError('Email')) ? 'is-invalid' : ''; ?>" id="email" value="<?= $users->Email; ?>" readonly>
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Email'); ?>
                </div>
              </div>

            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="Password">Password</label>
                <input type="password" name="Password" class="form-control <?= ($validation->hasError('Password')) ? 'is-invalid' : ''; ?>" id="Password" placeholder="Password tidak dapat di edit :)" readonly>
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Password'); ?>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="Repassword">Ketik Ulang Password</label>
                <input type="password" class="form-control <?= ($validation->hasError('Repassword')) ? 'is-invalid' : ''; ?>" name="Repassword" id="repassword" placeholder="Password tidak dapat di edit :)" readonly>
                <div id=" validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Repassword'); ?>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-1">
                <label for="Foto">Foto</label>
              </div>
              <div class="col-md-3">
                <img src="<?= base_url('assets/dataUsers/img/' . $users->Foto); ?>" class="img-thumbnail img-preview">
              </div>
              <div class="form-group col-md-8">
                <input type="file" name="Foto" id="Foto" class="form-control-file <?= ($validation->hasError('Foto')) ? 'is-invalid' : ''; ?>" id="Foto" onchange="previewImg()">
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Foto'); ?>
                </div>
                <label class="custom-file-label" for="Foto"><?= $users->Foto; ?></label>
              </div>

            </div>
            <div class="form-row mt-4">
              <div class="form-group col-md-6">
                <label for="Status">Status User</label>
                <select id="Status" name="Status" class="form-control <?= ($validation->hasError('Status')) ? 'is-invalid' : ''; ?>">
                  <?= ($users->Status == 'Aktif') ?
                    '<option value="Aktif">Aktif</option>
                     <option value="Tidak Aktif">Tidak Aktif</option>'
                    :
                    '<option value="Tidak Aktif">Tidak Aktif</option>
                    <option value="Aktif">Aktif</option>
                  '; ?>

                </select>
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Status'); ?>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="Role">Role</label>
                <select id="Role" name="Role" class="form-control <?= ($validation->hasError('Role')) ? 'is-invalid' : ''; ?>">
                  <?php foreach ($role as $u => $value) : ?>
                    <option value="<?= $value['Id_Role']; ?>" <?= ($value['Id_Role'] != $users->Role ? '' : 'selected') ?>><?= $value['Id_Role'];  ?> - <?= $value['Nama_Role']; ?></option>
                  <?php endforeach; ?>
                </select>
                <div id="validationServer03Feedback" class="invalid-feedback">
                  <?= $validation->getError('Role'); ?>
                </div>
              </div>
            </div>

          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <div class="card-tools">
              <div class="row">
                <div class="col-md-12">
                  <a href="<?= base_url('admin/users'); ?>" class="btn btn-secondary btn-sm text-white"><i class="fas fa-chevron-circle-left"></i> Kembali</a>
                  <button type="reset" class="btn btn-info btn-sm"><i class="fas fa-trash-restore"></i> Reset Data</button>
                  <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-save"></i> Update</button>
                </div>
              </div>
            </div>
          </div>
          <?= form_close(); ?>

        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
</section>