<section id="table-user">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Data User</h3>

            <div class="card-tools">
              <a href="<?= route_to('create_user'); ?>" class="btn btn-success btn-sm"><i class="fas fa-user-plus"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
            <!-- /.card-tools -->
          </div>
          <!-- /.card-header -->

          <!-- sweetalert start -->
          <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>
          <div class="ggl" data-ggl="<?= session()->get('message_errors'); ?>"></div>
          <!-- sweetalert end -->

          <div class="card-body">
            <div class="container">
              <table id="tablecrud1" class="table table-responsive-md table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Nama Role</th>
                    <th>Foto</th>
                    <th scope="colgroup">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($users as $u => $value) {
                  ?>
                    <tr>
                      <td><?= $no++; ?></td>
                      <td><?= $value['Username']; ?></td>
                      <td><?= $value['Email']; ?></td>
                      <td><?= $value['Status']; ?></td>
                      <td><?= $value['Nama_Role']; ?></td>
                      <td><?=
                            ($value['Foto'] == NULL) ?
                              '<img src="' . base_url('assets/dataUsers/img/user-default.png') . '"class="img-circle" height="50px" width ="50px" alt="Image User">'
                              :
                              '<img src="' . base_url('assets/dataUsers/img/' . $value['Foto']) . '" class="img-circle" height="50px" width="50px" alt="Image User">';
                          ?>
                      </td>
                      <td>
                        <div class="btn-group d-inline-flex">
                          <a href="<?= base_url('/admin/users/edit/' . $value['Id_User']) ?>" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-sm text-white"><i class="fas fa-search"></i></a>
                          <button data-toggle="modal" data-target="#hapusdata<?= $value['Id_User']; ?>" data-tooltip="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-sm btn-default"><i class="far fa-trash-alt"></i></button>
                        </div>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!-- /.card -->
    </div>

    <!-- Modal -->
    <?php foreach ($users as $key => $value) { ?>
      <div class="modal fade" id="hapusdata<?= $value['Id_User']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body justify-content-center">
              <h5>Apakah anda yakin untuk menghapus data ini?</h5>
            </div>

            <div class="modal-footer">
              <form name="formHapus" data-id="<?= $value['Id_User']; ?>" action="<?= base_url('/admin/users/' . $value['Id_User'] . ''); ?>" method="POST">
                <?= csrf_field(); ?>
                <input type="hidden" name="_method" value="DELETE">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</button>
                <button type="submit" class="btn btn-danger btn-sm">Ya hapus !!</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
</section>