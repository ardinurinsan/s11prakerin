<section id="dashboard-section">
  <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>

  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?= $jmlh_pengajuan; ?></h3>

            <p>Pengajuan Prakerin</p>
          </div>
          <div class="icon">
            <i class="fas fa-list-ul"></i>
          </div>
          <a href="/hubin/list-pengajuan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?= $jmlh_perusahaan; ?></h3>

            <p>Perusahaan</p>
          </div>
          <div class="icon">
            <i class="far fa-building"></i>
          </div>
          <a href="/hubin/perusahaan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3><?= $jmlh_siswa; ?></h3>

            <p>Siswa</p>
          </div>
          <div class="icon">
            <i class="fas fa-user-graduate"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->

      <!-- ./col -->
    </div>
    <div class="row justify-content-center">
      <div class="col-md-12 text-center">
        <img src="<?= base_url('assets/image/logo-img/business.svg') ?>" height="450px" alt="">
      </div>
    </div>
  </div>


</section>