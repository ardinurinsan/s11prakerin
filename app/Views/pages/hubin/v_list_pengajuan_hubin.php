<section id="list-pengajuan">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-outline card-info">
          <div class="card-header">
            <h3 class="card-title">Daftar Siswa yang mengajukan tempat Prakerin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body" style="display: block;">
            <table class="table table-sm table-responsive" id="tabeldata">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Siswa</th>
                  <th>Nama Perusahaan yang di ajukan</th>
                  <th>Nama Pimpinan Perusahaan</th>
                  <th>Jabatan dari pimpinan perusahaan</th>
                  <th>Email Perusahaan</th>
                  <th>Kontak Perusahan</th>
                  <th>Alamat Perusahaan</th>
                  <th>Status Pengajuan</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($pengajuan as $p => $value) :
                ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $value['Nama_Siswa']; ?></td>
                    <td><?= $value['Nama_Perusahaan']; ?></td>
                    <td><?= $value['Nama_Pimpinan_Perusahaan']; ?></td>
                    <td><?= $value['Jabatan']; ?></td>
                    <td><?= $value['Email_Perusahaan']; ?></td>
                    <td><?= $value['Kontak_Perusahaan']; ?></td>
                    <td><?= $value['Alamat_Perusahaan']; ?></td>
                    <td>
                      <!-- <div class="btn-group" role="group"> -->
                      <?php
                      $cekSet = $value['Status_Pengajuan'];
                      if ($cekSet == 0) {
                        $colorButton = 'btn btn-warning';
                      } elseif ($cekSet == 1) {
                        $colorButton = 'btn btn-success';
                      } elseif ($cekSet == 2) {
                        $colorButton = 'btn btn-danger';
                      }
                      ?>
                      <select class="<?= $colorButton; ?> setstatus" data-id="<?= $value['Id_SP']; ?>" data-nis="<?= $value['NIS']; ?>">
                        <?php if ($cekSet == 0) { ?>
                          <option class="bg-warning" value="0" <?= ($cekSet == 0) ? 'selected="selected"' : ''; ?>>Diperiksa</option>
                          <option class="bg-success" value="1" <?= ($cekSet == 1) ? 'selected="selected"' : ''; ?>>Diterima</option>
                          <option class="bg-danger" value="2" <?= ($cekSet == 2) ? 'selected="selected"' : ''; ?>>Ditolak</option>
                        <?php } ?>
                        <?php if ($cekSet == 1) { ?>
                          <option class="bg-success" value="1" <?= ($cekSet == 1) ? 'selected="selected"' : ''; ?>>Diterima</option>
                        <?php } ?>
                        <?php if ($cekSet == 2) { ?>
                          <option class="bg-danger" value="2" <?= ($cekSet == 2) ? 'selected="selected"' : ''; ?>>Ditolak</option>
                        <?php } ?>

                      </select>
                      <!-- </div> -->
                    </td>
                    <td>
                      <div class="btn-group d-inline-flex">
                        <button data-toggle="modal" id="btn-detail-listpengajuan" data-target="#detail" data-tooltip="tooltip" data-placement="top" title="Detail" class="btn btn-info btn-sm text-white" data-namasiswa="<?= $value['Nama_Siswa']; ?>" data-kelassiswa="<?= $value['Kelas']; ?>" data-jurusan="<?= $value['Nama_Jurusan']; ?>" data-jk_siswa="<?= $value['JK_Siswa']; ?>" data-kontak_siswa="<?= $value['Kontak_Siswa']; ?>" data-nama_perusahaan="<?= $value['Nama_Perusahaan']; ?>" data-nama_pimpinan_perus="<?= $value['Nama_Pimpinan_Perusahaan']; ?>" data-jabatan="<?= $value['Jabatan']; ?>" data-emailperus="<?= $value['Email_Perusahaan']; ?>" data-kontak_perusahaan="<?= $value['Kontak_Perusahaan']; ?>" data-alamat="<?= $value['Alamat_Perusahaan']; ?>" data-status="<?= $value['Status_Pengajuan']; ?>" data-kodepos="<?= $value['Kode_Pos']; ?>" data-tglawal="<?= $value['Tglbulan_Awal']; ?>" data-tglakhir="<?= $value['Tglbulan_Akhir']; ?>" data-kotaperus="<?= $value['Kota_Perusahaan']; ?>" data-website="<?= $value['Website_Perusahaan']; ?>" data-bidang="<?= $value['Bidang_Perusahaan']; ?>">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- MODAL DETAIL AWAL -->
  <div class="modal fade" id="detail" tabindex="-1" aria-labelledby="detailListpengajuan" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h5 class="modal-title" id="detailListpengajuan">Detail Pengajuan tempat Prakerin</h5>
          <button type="button" class="close text-white no-border" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="container-fluid">
            <div class="row">
              <div class="col">
                <h5 class="text-muted">* Data Siswa Pengajuan</h5>
                <table>
                  <tr>
                    <td>Nama Siswa</td>
                    <td>:</td>
                    <td><span id="namasiswa"></span></td>
                  </tr>
                  <tr>
                    <td>Kelas</td>
                    <td>:</td>
                    <td><span id="kelassiswa"></span></td>
                  </tr>
                  <tr>
                    <td>Jurusan</td>
                    <td>:</td>
                    <td><span id="jurusan"></span></td>
                  </tr>
                  <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><span id="jk_siswa"></span></td>
                  </tr>
                  <tr>
                    <td>Nomor Telepon</td>
                    <td>:</td>
                    <td><span id="kontak_siswa"></span></td>
                  </tr>
                </table>
                <hr class="hr">
                <table>
                  <tr>
                    <td>Nama Perusahaan</td>
                    <td>:</td>
                    <td id="nama_perusahaan"></td>
                  </tr>
                  <tr>
                    <td>Bidang Perusahaan</td>
                    <td>:</td>
                    <td id="nama_bidang_perus"></td>
                  </tr>
                  <tr>
                    <td>Nama Pimpinan Perusahaan</td>
                    <td>:</td>
                    <td id="nama_pimpinan_perus"></td>
                  </tr>
                  <tr>
                    <td>Jabatan Pimpinan</td>
                    <td>:</td>
                    <td id="jabatan"></td>
                  </tr>
                  <tr>
                    <td>Email Perusahaan</td>
                    <td>:</td>
                    <td id="emailperus"></td>
                  </tr>
                  <tr>
                    <td>Kontak Perusahaan</td>
                    <td>:</td>
                    <td id="kontak_perusahaan"></td>
                  </tr>
                  <tr>
                    <td>Alamat Perusahaan</td>
                    <td>:</td>
                    <td id="alamat"></td>
                  </tr>
                  <tr>
                    <td>Website Perusahaan</td>
                    <td>:</td>
                    <td id="website"></td>
                  </tr>
                  <tr>
                    <td>Kota Perusahaan</td>
                    <td>:</td>
                    <td id="kota"></td>
                  </tr>
                  <tr>
                    <td>Kode Pos Perusahaan</td>
                    <td>:</td>
                    <td id="kodepos"></td>
                  </tr>
                  <tr>
                    <td>Tanggal Pengajuan Awal</td>
                    <td>:</td>
                    <td id="tglawal"></td>
                  </tr>
                  <tr>
                    <td>Tanggal Pengajuan Akhir</td>
                    <td>:</td>
                    <td id="tglakhir"></td>
                  </tr>
                </table>
              </div>
              <div class="col-5">
                <!-- <h1>detail</h1> -->
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fas fa-times"></i> Tutup</button>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL DETAIL AKHIR -->

</section>

<script>
  $("#tabeldata").on("change", ".setstatus", function() {
    // your code goes here

    Swal.fire({
      title: 'Apakah anda yakin?',
      text: 'Ingin mengubah status persetujuan ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#05b508',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya!',
      cancelButtonText: 'Tidak!'
    }).then((result) => {
      if (result.isConfirmed) {
        var id_sp = $(this).attr('data-id');
        var nis = $(this).attr('data-nis');
        var valstatus = $(this).val();

        $.ajax({
          type: 'post',
          data: {
            id_sp: id_sp,
            nis: nis,
            status: valstatus
          },
          dataType: 'json',
          url: 'list-pengajuan/proses_update_status',
          success: function() {
            location.reload();
          }
        });


        Swal.fire(
          'Berhasil!',
          'Penggantian status berhasil.',
          'success'
        )
      }
    })
  });


  $(document).ready(function() {
    $('#tabeldata').DataTable({
      dom: 'Blfrtip',
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
      buttons: [
        'copy',
        'excel',
        'print'
      ]
    });
  });
</script>