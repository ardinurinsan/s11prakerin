<section>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-9">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Perusahaan</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body" style="display: block;">
            <?php
            echo form_open(route_to('procreate_perus'));
            helper('text');
            $kodeperus = random_string('alnum', 5)
            ?>
            <input type="hidden" name="Id_Perusahan" value="<?= $kodeperus; ?>">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="namaP">Nama Perusahaan</label>
                <input type="text" name="namaperus" class="form-control <?= ($validation->hasError('namaperus')) ? 'is-invalid' : ''; ?>" id="namaP" placeholder="Nama perusahaan" value="<?= old('namaperus'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('namaperus'); ?>
                </div>
              </div>

              <div class="form-group col-md-6">
                <label for="webprus">Website Perusahaan (Opsioanal)</label>
                <input type="text" name="webperus" class="form-control <?= ($validation->hasError('webperus')) ? 'is-invalid' : ''; ?>" id="webprus" placeholder="www.contoh.com" value="<?= old('webperus'); ?>">
              </div>

            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="pimperus">Nama Pimpinan</label>
                <input type="text" name="pimperus" class="form-control <?= ($validation->hasError('pimperus')) ? 'is-invalid' : ''; ?>" id="pimperus" placeholder="Nama pimpinan" value="<?= old('pimperus'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('pimperus'); ?>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="jabatan">Jabatan Pimpinan</label>
                <input type="text" class="form-control <?= ($validation->hasError('jabpimperus')) ? 'is-invalid' : ''; ?>" name="jabpimperus" id="jabatan" placeholder="jabatan" value="<?= old('jabpimperus'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('jabpimperus'); ?>
                </div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="alamat">Alamat Perusahaan</label>
                <textarea class="form-control <?= ($validation->hasError('alamatperus')) ? 'is-invalid' : ''; ?>" name="alamatperus" id="alamat" placeholder="Alamat"><?= old('alamatperus'); ?></textarea>
                <div class="invalid-feedback">
                  *<?= $validation->getError('alamatperus'); ?>
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="koperus">Kontak Perusahaan</label>
                <input type="number" name="koperus" class="form-control <?= ($validation->hasError('koperus')) ? 'is-invalid' : ''; ?>" id="koperus" placeholder="kontak perusahaan" value="<?= old('koperus'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('alamatperus'); ?>
                </div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-5">
                <label for="email">Email Perusahaan</label>
                <input type="email" name="email" class="form-control <?= ($validation->hasError('email')) ? 'is-invalid' : ''; ?>" id="email" placeholder="contoh@gmail.com" value="<?= old('email'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('alamatperus'); ?>
                </div>
              </div>
              <div class="form-group col-md-4">
                <label for="kotperus">Kota Perusahaan</label>
                <input type="text" name="kotperus" class="form-control <?= ($validation->hasError('kotperus')) ? 'is-invalid' : ''; ?>" id="kotperus" placeholder="Kota Perusahaan" value="<?= old('kotperus'); ?>">
                <div class="invalid-feedback">
                  *<?= $validation->getError('kotperus'); ?>
                </div>
              </div>
              <div class="form-group col-md-3">
                <label for="inputZip">Kode Pos (Opsioanal)</label>
                <input type="text" class="form-control" name="kodepos" id="inputZip" placeholder="Kode Pos" value="<?= old('kodepos'); ?>">
              </div>
            </div>
            <div class="card-footer">
              <div class="card-tools">
                <div class="row">
                  <div class="col-md-12">
                    <a href="/hubin/perusahaan" class="btn btn-secondary btn-sm text-white"><i class="fas fa-chevron-circle-left"></i> Kembali</a>
                    <button type="reset" class="btn btn-info btn-sm"><i class="fas fa-trash-restore"></i> Reset Data</button>
                    <button type="submit" href="" class="btn btn-success btn-sm"><i class="fas fa-save"></i> Save</button>
                  </div>
                </div>
              </div>
            </div>
            <?= form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>