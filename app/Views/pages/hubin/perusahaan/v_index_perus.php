<section id="section-perusahaan">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="card card-outline card-primary">
          <!-- sweetalert start -->
          <div class="swal" data-swal="<?= session()->get('message_success'); ?>"></div>
          <div class="ggl" data-ggl="<?= session()->get('message_errors'); ?>"></div>
          <!-- sweetalert end -->
          <div class="card-header">
            <h3 class="card-title">Data Perusahaan yang berkerja sama dengan SMK Negeri 11 Bandung</h3>
            <div class="card-tools">
              <a href="<?= route_to('create_perus'); ?>" type="button" class="btn btn-sm bg-success"><i class="far fa-building">&nbsp;&nbsp;</i><i class="fas fa-plus"></i></a>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body" style="display: block;">
            <table class="table table-responsive table-sm table-hover display" id="tabeldata">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Perusahaan</th>
                  <th>Nama Pimpinan Perusahaan</th>
                  <th>Jabatan</th>
                  <th>Alamat Perusahaan</th>
                  <th>Email</th>
                  <th>Website</th>
                  <th>Kontak</th>
                  <th>Kode Pos</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($perusahaan as $p => $value) :
                ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $value['Nama_Perusahaan']; ?></td>
                    <td><?= $value['Nama_Pimprus']; ?></td>
                    <td><?= $value['Jabatan_Pimprus']; ?></td>
                    <td><?= $value['Alamat_Perusahaan']; ?></td>
                    <td><?= $value['Email_Perusahaan']; ?></td>
                    <td><?= $value['Website']; ?></td>
                    <td><?= $value['Kontak_Perusahaan']; ?></td>
                    <td><?= $value['Kode_Pos']; ?></td>
                    <td>
                      <div class="btn-group">
                        <div class="btn-group d-inline-flex">
                          <a href="<?= base_url('hubin/perusahaan/edit/' . $value['Id_Perusahaan']); ?>" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>

                          <button type="button" data-target="#detail" data-toggle="modal" data-tooltip="tooltip" data-placement="top" title="Detail" class="btn btn-warning btn-sm text-white" data-namaPer="<?= $value['Nama_Perusahaan']; ?>" data-namaPimper="<?= $value['Nama_Pimprus']; ?>" data-jabpim="<?= $value['Jabatan_Pimprus']; ?>" data-alperus="<?= $value['Alamat_Perusahaan']; ?>" data-eperus="<?= $value['Email_Perusahaan']; ?>" data-web="<?= $value['Website']; ?>" data-koperus="<?= $value['Kontak_Perusahaan']; ?>" data-kodpos="<?= $value['Kode_Pos']; ?>" id="btn-detail-perusahaan"><i class="fas fa-search"></i></button>

                          <button data-toggle="modal" data-target="#hapusdata<?= $value['Id_Perusahaan']; ?>" data-tooltip="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-sm btn-default"><i class="far fa-trash-alt"></i></button>
                        </div>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- MODAL DELETE AWAL-->
<?php foreach ($perusahaan as $key => $value) { ?>
  <div class="modal fade" id="hapusdata<?= $value['Id_Perusahaan']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body justify-content-center">
          <h5>Apakah anda yakin untuk menghapus data ini?</h5>
        </div>

        <div class="modal-footer">
          <form name="formHapus" action="<?= base_url('/hubin/perusahaan/' . $value['Id_Perusahaan'] . ''); ?>" method="POST">
            <?= csrf_field(); ?>
            <input type="hidden" name="_method" value="DELETE">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Kembali</button>
            <button type="submit" class="btn btn-danger btn-sm">Ya hapus !!</button>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
<!-- MODAL DELETE AKHIR-->

<!-- MODAL DETAIL AWAL -->
<div class="modal fade" id="detail" tabindex="-1" aria-labelledby="detailListpengajuan" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h5 class="modal-title" id="detailListpengajuan">Detail Perusahaan</h5>
        <button type="button" class="close text-white no-border" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <h5 class="text-muted">* Data Perusahaan</h5>
              <table>
                <tr>
                  <td>Nama Perusahaan</td>
                  <td>:</td>
                  <td><span id="namaper"></span></td>
                </tr>
                <tr>
                  <td>Nama Pimpinan Perusahaan</td>
                  <td>:</td>
                  <td><span id="namapimper"></span></td>
                </tr>
                <tr>
                  <td>Jabatan</td>
                  <td>:</td>
                  <td><span id="jabpim"></span></td>
                </tr>
                <tr>
                  <td>Alamat Perusahaan</td>
                  <td>:</td>
                  <td><span id="alperus"></span></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>:</td>
                  <td><span id="eperus"></span></td>
                </tr>
                <tr>
                  <td>Website</td>
                  <td>:</td>
                  <td><span id="web"></span></td>
                </tr>
                <tr>
                  <td>Kontak</td>
                  <td>:</td>
                  <td><span id="koperus"></span></td>
                </tr>
                <tr>
                  <td>Kode Pos</td>
                  <td>:</td>
                  <td><span id="kodpos"></span></td>
                </tr>
              </table>
              <hr class="hr">
            </div>
            <div class="col-5">
              <!-- <h1>detail</h1> -->
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="fas fa-times"></i> Tutup</button>
      </div>
    </div>
  </div>
</div>
<!-- MODAL DETAIL AKHIR -->

<script>
  $(document).ready(function() {
    $('#tabeldata').DataTable({
      dom: 'Blfrtip',
      dom: '<"float-left"B><"float-right"f>rt<"row"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
      buttons: [
        'copy',
        'excel',
        'print'
      ]
    });
  });

  $(document).on('click', '#btn-detail-perusahaan', function() {
    $('#namaper').text($(this).data('namaper'));
    $('#namapimper').text($(this).data('namapimper'));
    $('#jabpim').text($(this).data('jabpim'));
    $('#alperus').text($(this).data('alperus'));
    $('#eperus').text($(this).data('eperus'));
    $('#web').text($(this).data('web'));
    $('#koperus').text($(this).data('koperus'));
    $('#kodpos').text($(this).data('kodpos'));
  })
</script>