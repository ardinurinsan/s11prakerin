<section id="add-siswa">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card card-outline card-success">
          <div class="card-header">
            <h3 class="card-title">Edit Data Siswa</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <?= form_open(base_url('hubin/siswa/process_update/' . $siswa->NIS)); ?>
          <input type="hidden" name="NIS" value="<?= $siswa->NIS; ?>">
          <div class="card-body">
            <div class="form-group">
              <label for="nis">NIS</label>
              <input type="number" pattern="[0-10]" class="form-control <?= ($validation->hasError('NIS')) ? 'is-invalid' : ''; ?>" value="<?= old('NIS', $siswa->NIS); ?>" id="nis" name="NIS" placeholder="Masukkan NIS">
              <div class="invalid-feedback">
                *<?= $validation->getError('NIS'); ?>
              </div>
            </div>
            <div class="form-group">
              <label for="namasis">Nama Siswa</label>
              <input type="text" class="form-control  <?= ($validation->hasError('Nama_Siswa')) ? 'is-invalid' : ''; ?>" value="<?= old('Nama_Siswa', $siswa->Nama_Siswa); ?>" id="namasis" name="Nama_Siswa" placeholder="Masukan Nama Siswa">
              <div class="invalid-feedback">
                *<?= $validation->getError('Nama_Siswa'); ?>
              </div>
            </div>

            <div class="row">
              <div class="col-md-5 col-sm-4">
                <div class="form-group">
                  <label for="kelas">Kelas</label>
                  <div class="input-group">
                    <select name="Kelas" id="kelas" class="form-control <?= ($validation->hasError('Kelas')) ? 'is-invalid' : ''; ?>">
                      <?= ($siswa->Kelas == '11') ?
                        '<option value="11">11</option>
                     <option value="12">12</option>'
                        :
                        '<option value="12">12</option>
                    <option value="11">11</option>
                  '; ?>
                    </select>
                    <div class="invalid-feedback">
                      *<?= $validation->getError('Kelas'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <label class="col-form-label">Jenis Kelamin :</label> <br>
                <input type="radio" name="JK_Siswa" <?= $siswa->JK_Siswa == "L" ? 'checked' : ''; ?> class="form-control-sidebar-animate <?= ($validation->hasError('JK_Siswa')) ? 'is-invalid' : ''; ?>" value="L"> L<br>
                <input type="radio" name="JK_Siswa" <?= $siswa->JK_Siswa == "P" ? 'checked' : ''; ?> class="form-control-sidebar-animate <?= ($validation->hasError('JK_Siswa')) ? 'is-invalid' : ''; ?>" value="P"> P
                <div class="invalid-feedback">
                  *<?= $validation->getError('JK_Siswa'); ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="jurus">Jurusan</label>
                  <div class="input-group">
                    <select name="Id_Jurusan" id="jurus" class="form-control <?= ($validation->hasError('Id_Jurusan')) ? 'is-invalid' : ''; ?>">
                      <option value="">***Pilih Jurusan***</option>
                      <?php foreach ($jurusan as $j => $value) : ?>
                        <option value="<?= $value['Id_Jurusan']; ?>" <?= ($value['Id_Jurusan'] != $siswa->Id_Jurusan ? '' : 'selected') ?>><?= $value['Nama_Jurusan']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback">
                      *<?= $validation->getError('Id_Jurusan'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-8">
                <label for="jurus">No HP</label>
                <div class="input-group">
                  <input type="text" name="Kontak_Siswa" class="form-control <?= ($validation->hasError('Kontak_Siswa')) ? 'is-invalid' : ''; ?>" id="noHp" placeholder="No Hp" value="<?= $siswa->Kontak_Siswa; ?>" min="1"> <br>
                  <small class="text-warning">*Tuliskan Awalan no hp dengan tanpa awalan 0 atau +62</small>
                  <div class="invalid-feedback">
                    *<?= $validation->getError('Kontak_Siswa'); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <div class="input-group">
                    <textarea id="alamat" class="form-control <?= ($validation->hasError('Alamat')) ? 'is-invalid' : ''; ?>" name="Alamat" rows="2"><?= old('Alamat', $siswa->Alamat); ?></textarea>
                    <div class="invalid-feedback">
                      *<?= $validation->getError('Alamat'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <div class="row">
              <div class="col-md-12">
                <a href="<?= base_url('hubin/siswa'); ?>" class="btn btn-secondary btn-sm text-white"><i class="fas fa-chevron-circle-left"></i> Kembali</a>
                <button type="reset" class="btn btn-info btn-sm"><i class="fas fa-trash-restore"></i> Reset Data</button>
                <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-save"></i> Save</button>
              </div>
            </div>
          </div>
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</section>