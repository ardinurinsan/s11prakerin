<?php

namespace App\Controllers;

class Pages extends BaseController
{
	//-----------------------HOME---------------------------------------
	public function index()
	{
		$data = ['title' => 'S11Prakerin | SMK Negeri 11 Bandung'];
		return view('v_home', $data);
	}
	//--------------------------------------------------------------------

	//--------------------------------------------------------------------
	public function maintenance()
	{
		$data = [
			'title'       => 'Sistem | Maintenance',
			'judul'       => '',
			'methodName'  => '',
			'isi'         => 'error/v_maintenance'
		];
		return view('layout/backend/component/v_wrapper', $data);
	}
	//--------------------------------------------------------------------

}
