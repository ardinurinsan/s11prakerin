<?php

namespace App\Controllers\Sadmin\mstPerus;

use App\Controllers\BaseController;
use App\Models\Sadmin\perusM;

class HomePerusM extends BaseController
{
  public function __construct()
  {
    $this->perusM = new perusM();
  }

  //--------------------V_INDEX------------------------------------------
  public function index()
  {
    $data = [
      'title'      => 'Tabel Master | Perusahaan',
      'judul'      => 'Tabel Perusahaan',
      'methodName' => 'dataMaster',
      'perusahaan' => $this->perusM->getPerus(),
      'isi'        => 'pages/superAdmin/perusahaan/v_index_perusM'
    ];
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //--------------------CREATE------------------------------------------
  //--------------------------------------------------------------------
  //--------------------UPDATE------------------------------------------
  //--------------------------------------------------------------------
  //--------------------DELETE------------------------------------------
  //--------------------------------------------------------------------
}
