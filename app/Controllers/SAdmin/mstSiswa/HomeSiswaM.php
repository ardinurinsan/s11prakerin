<?php

namespace App\Controllers\Sadmin\mstSiswa;

use App\Controllers\BaseController;
use App\Models\Sadmin\siswaM;
use App\Models\Sadmin\jurusanM;

class HomeSiswaM extends BaseController
{

  public function __construct()
  {
    $this->siswaM = new siswaM();
    $this->jurusanM = new jurusanM();
    helper('form');
  }

  //--------------------V_INDEX-----------------------------------------
  public function index()
  {
    $data = [
      'title'       => 'Tabel Master | Siswa',
      'judul'       => 'Tabel Siswa',
      'methodName'  => 'dataMaster',
      'siswa'       =>  $this->siswaM->getSiswa(),
      'isi'         => 'pages/superAdmin/siswa/v_index_siswaM'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //--------------------CREATE------------------------------------------
  public function create()
  {
    $data = [
      'title'      => 'Siswa | Tambah',
      'judul'      => 'Tambah Data Siswa',
      'methodName' => 'dataMaster',
      'jurusan'    => $this->jurusanM->getJurusan(),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/superAdmin/siswa/v_create_siswaM'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function process_save()
  {
    //validasi
    if ($this->validate([
      'NIS' => [
        'label' => 'NIS',
        'rules'  => 'required|is_unique[mst_siswa.NIS]|max_length[10]|numeric',
        'errors' => [
          'required'   => '{field} Tidak boleh kosong !!!',
          'is_unique'  => '{field} Sudah terdaftar !!!',
          'max_length' => '{field} Maximal 10 inputan !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Nama_Siswa' => [
        'label'  => 'Nama Siswa',
        'rules'  => 'required|alpha_space',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'alpha_space' => '{field} Harus berupa huruf!!!',
        ]
      ],
      'Kelas' => [
        'label'  => 'Kelas',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'JK_Siswa' => [
        'label'  => 'Jenis Kelamin',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Id_Jurusan' => [
        'label'  => 'Jurusan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Kontak_Siswa' => [
        'label'  => 'No Kontak',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Alamat' => [
        'label'  => 'Alamat',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],

    ])) {
      //is valid
      $noInputan       = "+62" . $this->request->getPost('Kontak_Siswa') . "";
      $jumlahPengajuan = "1";
      $intPengajuan    = intval($jumlahPengajuan);
      $data = array(
        'NIS'         => $this->request->getPost('NIS'),
        'Nama_Siswa'  => $this->request->getPost('Nama_Siswa'),
        'Kelas'       => $this->request->getPost('Kelas'),
        'JK_Siswa'    => $this->request->getPost('JK_Siswa'),
        'Kontak_Siswa' =>  $noInputan,
        'Id_Jurusan'  => $this->request->getPost('Id_Jurusan'),
        'Alamat'      => $this->request->getPost('Alamat'),
        'Jumlah_Pengajuan'  => $intPengajuan,
      );

      //simpan data
      $this->siswaM->insert($data);

      session()->setFlashdata('message_success', 'Tambah data Siswa berhasil !!!');
      return redirect()->to(route_to('index_siswa'))->withInput();
    } else {
      //is invalid
      return redirect()->to(route_to('create_siswa'))->withInput();
    }
  }
  //--------------------------------------------------------------------
  //--------------------UPDATE------------------------------------------
  public function update($nis)
  {
    $data = [
      'title'      => 'Siswa | Edit',
      'judul'      => 'Edit Data Siswa',
      'methodName' => 'dataMaster',
      'jurusan'    => $this->jurusanM->getJurusan(),
      'siswa'      => $this->siswaM->getDataSiswaBaris($nis),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/superAdmin/siswa/v_update_siswaM'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  public function proses_update_siswa($nis)
  {
    //validasi
    if ($this->validate([
      'NIS' => [
        'label' => 'NIS',
        'rules'  => 'required|max_length[10]|numeric',
        'errors' => [
          'required'   => '{field} Tidak boleh kosong !!!',
          'max_length' => '{field} Maximal 10 inputan !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Nama_Siswa' => [
        'label'  => 'Nama Siswa',
        'rules'  => 'required|alpha_space',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'alpha_space' => '{field} Harus berupa huruf!!!',
        ]
      ],
      'Kelas' => [
        'label'  => 'Kelas',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'JK_Siswa' => [
        'label'  => 'Jenis Kelamin',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Id_Jurusan' => [
        'label'  => 'Jurusan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Kontak_Siswa' => [
        'label'  => 'No Kontak',
        'rules'  => 'required|numeric',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Alamat' => [
        'label'  => 'Alamat',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],

    ])) {
      //is valid
      $data = array(
        'NIS'         => $this->request->getPost('NIS'),
        'Nama_Siswa'  => $this->request->getPost('Nama_Siswa'),
        'Kelas'       => $this->request->getPost('Kelas'),
        'JK_Siswa'    => $this->request->getPost('JK_Siswa'),
        'Kontak_Siswa' => $this->request->getPost('Kontak_Siswa'),
        'Id_Jurusan'  => $this->request->getPost('Id_Jurusan'),
        'Alamat'      => $this->request->getPost('Alamat'),
      );

      //simpan data
      $this->siswaM->update_data_siswa($data, $nis);

      session()->setFlashdata('message_success', 'Edit data Siswa berhasil !!!');
      return redirect()->to(route_to('index_siswa'))->withInput();
    } else {
      //is invalid
      return redirect()->to(base_url('admin/siswa/edit/' . $nis))->withInput();
    }
  }
  //--------------------------------------------------------------------
  //--------------------DELETE------------------------------------------
  public function delete($nis)
  {
    $this->siswaM->deleteSiswa($nis);
    session()->setFlashdata('message_success', 'Data siswa berhasil di hapus !!!');
    return redirect()->to(base_url(route_to('index_siswa')));
  }
  //--------------------------------------------------------------------

}
