<?php

namespace App\Controllers\SAdmin;

use App\Models\usersM;
use App\Models\roleM;
use App\Controllers\BaseController;

class Users extends BaseController
{

  public function __construct()
  {
    $this->userM = new usersM();
    $this->roleM = new roleM();
    helper('form');
  }

  //--------------------V_INDEX------------------------------------------
  public function index()
  {
    $data = [
      'title'       => 'Users',
      'judul'       => 'Akun User',
      'users'       => $this->userM->getDataUsers(),
      'methodName'  => 'users',
      'isi'         => 'pages/superAdmin/akun/v_index_user'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //---------------------------------------------------------------------

  //--------------------CREATE------------------------------------------
  public function create()
  {
    $data = [
      'title'       => 'User | Tambah',
      'judul'       => 'Tambah User',
      'methodName'  => 'users',
      'role'        => $this->roleM->getRole(),
      'validation'    => \Config\Services::validation(),
      'isi'         => 'pages/superAdmin/akun/v_add_user'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function proses_simpan()
  {
    //validasi
    if ($this->validate([
      'Username' => [
        'label' => 'Username',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Email' => [
        'label' => 'Email',
        'rules'  => 'required|Is_unique[users.Email]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'Is_unique'   => '{field} Ini sudah terdaftar !!!',
        ]
      ],
      'Password' => [
        'label' => 'Password',
        'rules'  => 'required|min_length[8]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'min_length'  => '{field} minimal 8 karakter !!!'
        ]
      ],
      'Repassword' => [
        'label' => 'Ketik Password',
        'rules'  => 'required|matches[Password]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'matches'  => '{field} Tidak sama !!!'
        ]
      ],
      'Role' => [
        'label' => 'Role',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Foto' => [
        'label'  => 'Foto',
        'rules'  => 'max_size[Foto,1024]|mime_in[Foto,image/png,image/jpg,image/jpeg]|is_image[Foto]',
        'errors' => [
          'max_size' => '{field} max 1 mb !!!',
          'mime_in'  => 'Upload {field} ini hanya bisa {field} ber ektensi PNG, JPG, JPEG !!!'
        ]
      ],
      'Status' => [
        'label'  => 'Status',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
    ])) {
      //is valid

      //ambil gambar
      $fileFoto = $this->request->getFile('Foto');
      if ($fileFoto->getError() == 4) {
        $namaFoto = 'user-default.png';
      } else {
        // generate nama random
        $namaFoto = $fileFoto->getRandomName();
        //pindahkan file 
        $fileFoto->move('assets/dataUsers/img',  $namaFoto);
      }


      $data = array(
        'Username' => $this->request->getPost('Username'),
        'Email'    => $this->request->getPost('Email'),
        'Password' => password_hash($this->request->getPost('Password'), PASSWORD_DEFAULT),
        'Role'     => $this->request->getPost('Role'),
        'Foto'     => $namaFoto,
        'Status'   => $this->request->getPost('Status'),
      );

      $this->userM->insert($data);

      session()->setFlashdata('message_success', 'Data berhasil di simpan !!!');
      return redirect()->to(route_to('users'));
    } else {
      //is invalid
      return redirect()->to(route_to('create_user'))->withInput();
    }
  }
  //--------------------------------------------------------------------

  //--------------------UPDATE------------------------------------------
  public function update($id_user)
  {
    $data = [
      'title'       => 'User | Edit',
      'judul'       => 'Edit User',
      'methodName'  => 'users',
      'role'        => $this->roleM->getRole(),
      'users'       => $this->userM->getDataUserBaris($id_user),
      'validation'  => \Config\Services::validation(),
      'isi'         => 'pages/superAdmin/akun/v_update_user'
    ];

    // jika request tidak ada di databse
    // if (empty($data['users'])) {
    //   throw new \CodeIgniter\Exceptions\PageNotFoundException('User ' . $id_user . ' tidak di temukan.');
    // }
    /* 
    * kemudian data akan di tembakkan ke v_wrapper yaitu sebagai template poliklinik pembungkus yang dinamis
    */
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  public function proses_update($id_user)
  {
    //validasi
    if ($this->validate([
      'Username' => [
        'label' => 'Username',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      // 'Password' => [
      //   'label' => 'Password',
      //   'rules'  => 'required|min_length[8]',
      //   'errors' => [
      //     'required'    => '{field} Tidak boleh kosong !!!',
      //     'min_length'  => '{field} minimal 8 karakter !!!'
      //   ]
      // ],
      // 'Repassword' => [
      //   'label' => 'Ketik Password',
      //   'rules'  => 'required|matches[Password]',
      //   'errors' => [
      //     'required'    => '{field} Tidak boleh kosong !!!',
      //     'matches'  => '{field} Tidak sama !!!'
      //   ]
      // ],
      'Role' => [
        'label' => 'Role',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Foto' => [
        'label'  => 'Foto',
        'rules'  => 'max_size[Foto,1024]|mime_in[Foto,image/png,image/jpg,image/jpeg]|is_image[Foto]',
        'errors' => [
          'max_size' => '{field} max 1 mb !!!',
          'mime_in'  => 'Upload {field} ini hanya bisa {field} ber ektensi PNG, JPG, JPEG !!!'
        ]
      ],
      'Status' => [
        'label'  => 'Status',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
    ])) {
      //is valid
      //ambil gambar
      $fileFoto = $this->request->getFile('Foto');
      //cek gambar, apakah tetep gambar lama
      if ($fileFoto->getError() == 4) {
        $namaFoto = $this->request->getVar('FotoLama');
      } else {
        //generate nama file random
        $namaFoto = $fileFoto->getRandomName();
        //pindah gambar
        $fileFoto->move('assets/dataUsers/img',  $namaFoto);
        //hapus file yang lama
        // unlink('assets/dataUsers/img/' .  $this->request->getVar('FotoLama'));
      }

      $data = array(
        'Id_User'  => $id_user,
        'Username' => $this->request->getPost('Username'),
        'Email'    => $this->request->getPost('Email'),
        // 'Password' => password_hash($this->request->getPost('Password'), PASSWORD_DEFAULT),
        'Role'   => $this->request->getPost('Role'),
        'Foto'   => $namaFoto,
        'Status' => $this->request->getPost('Status'),
      );

      $this->userM->update_data_user($data, ['Id_User' => $id_user]);
      session()->setFlashdata('message_success', 'Data berhasil di simpan !!!');
      return redirect()->to(route_to('users'));
    } else {
      //is invalid
      return redirect()->to(base_url('admin/users/edit/' . $id_user))->withInput();
    }
  }
  //--------------------------------------------------------------------

  //--------------------DELETE------------------------------------------
  public function delete($id_user)
  {

    //cari gambar berdasarkan id
    $dataUser = $this->userM->getDataUserBaris($id_user);

    //cek jika file gambar default jangan di hapus
    if ($dataUser->Foto != 'user-default.png') {
      # code...
      //hapus gambar
      unlink('assets/dataUsers/img/' . $dataUser->Foto);
    }
    if (session()->get('Id_User') == $id_user) {
      session()->setFlashdata('message_errors', 'Data user Tidak bisa di hapus karena ini adalah data anda!!!');
      return redirect()->to(base_url(route_to('users')));
    }

    $this->userM->deleteUser($id_user);
    session()->setFlashdata('message_success', 'Data user berhasil di hapus !!!');
    return redirect()->to(base_url(route_to('users')));
  }
  //--------------------------------------------------------------------
}
