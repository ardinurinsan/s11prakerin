<?php

namespace App\Controllers\SAdmin;

use App\Controllers\BaseController;

class HomeAdmin extends BaseController
{

  public function index()
  {
    $data = [
      'title' => 'Dashboard | Admin',
      'judul' => 'Dashboard',
      'methodName' => 'Dashboard',
      'isi'   => 'pages/superAdmin/v_index_admin'
    ];
    if (session()->get('Role') != 1 and session()->get('login') == true) {
      if (session()->get('Role') == 2) {
        return redirect()->to(base_url('hubin'));
      } elseif (session()->get('Role') == 3) {
        return redirect()->to(base_url('siswa'));
        // return '<script>alert(\'anda tersesat\')</script>';
      } else {
        echo 'Anda tersesat!!!';
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function add()
  {
    return "hallo";
  }
}
