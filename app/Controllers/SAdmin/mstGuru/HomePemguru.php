<?php

namespace App\Controllers\Sadmin\mstGuru;

use App\Controllers\BaseController;

class HomePemguru extends BaseController
{

  public function index()
  {
    $data = [
      'title'       => 'Tabel Master | Guru',
      'judul'       => 'Tabel Master Pembimbing Guru',
      'methodName'  => 'dataMaster',
      'isi'         => 'pages/superAdmin/guru/v_index_guru'
    ];
    return view('layout/backend/component/v_wrapper', $data);
  }
}
