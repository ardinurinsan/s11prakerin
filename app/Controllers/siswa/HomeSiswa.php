<?php

namespace App\Controllers\Siswa;

use App\Controllers\BaseController;
use App\Models\Hubin\hubinM;
use App\Models\Sadmin\perusM;
use App\Models\UsersM;
use App\Models\Sadmin\siswaM;
use App\Models\Siswa\pengajuanModel;
use Config\App;

class HomeSiswa extends BaseController
{

  public function __construct()
  {
    $this->siswaM = new siswaM();
    $this->usersM = new usersM();
    $this->perusM = new perusM();
    $this->pengajuanM = new pengajuanModel();
    $this->hubinM = new hubinM();
    helper('form');
  }

  //----------------V_Index---------------------------------------------
  public function index()
  {
    $email = session()->get('siswa')->Email;
    $data = [
      'title'       => 'Dashboard | Siswa',
      'judul'       => '',
      'methodName'  => 'Dashboard',
      'isi'         => 'pages/siswa/v_index_siswa',
      'jmlh_perusahaan' => $this->perusM->countAllResults(),
      'siswa'       => $this->usersM->getDataSiswa($email),
      'pengajuan'  => $this->hubinM->getListDataPengajuan(),

    ];
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //----------------V_Profile-------------------------------------------
  public function profile()
  {
    $email = session()->get('siswa')->Email;
    $data = [
      'title'       => 'Profile | Siswa',
      'judul'       => 'Profile',
      'methodName'  => 'profile',
      'validation'  => \Config\Services::validation(),
      'siswa'       => $this->usersM->getDataSiswa($email),
      'isi'         => 'pages/siswa/v_profile_siswa'
    ];
    return view('layout/backend/component/v_wrapper', $data);
  }

  //-----------------Pengajuan------------------------------------------
  public function pengajuan()
  {
    $nis = session()->get('siswa')->NIS;
    $data = [
      'title'       => 'Pengajuan | Siswa',
      'judul'       => 'Pengajuan Tempat Prakerin',
      'methodName'  => 'pengajuan',
      'siswa'       => $this->siswaM->getDataSiswaBaris($nis),
      'pengajuan'   => $this->pengajuanM->getDataPengajuanSama($nis),
      'isi'         => 'pages/siswa/v_pengajuan_siswa'
    ];
    return view('layout/backend/component/v_wrapper', $data);
  }
  //proses simpan pengajuan
  public function proses_save_pengajuan()
  {
    //ambil nis user dari input hidden
    $getNis = $this->request->getPost('NIS');
    //ambil semua data dari nis user
    $rowSiswa = $this->siswaM->getDataSiswaBaris($getNis);
    //cek jumlah pengajuan
    $jumlahPengajuan = $rowSiswa->Jumlah_Pengajuan;

    //logika if jika lebih besar dari 0 maka eksekusi
    if ($jumlahPengajuan > 0) {
      //kurangi jumlah pengajuan di database
      $kurangi = $jumlahPengajuan - 1;
      $dataS   = array(
        'Jumlah_Pengajuan' => $kurangi,
      );
      $this->siswaM->update_data_siswa($dataS, $getNis);

      //membuat data otomatis yang nantinya nya di simpan ke array
      $statusPengajuan = "Proses Pemeriksaan";

      $data = array(
        'Id_SP'                   => $this->request->getPost('Id_SP'),
        'NIS'                     => $getNis,
        'Nama_Perusahaan'         => $this->request->getPost('Nama_Perusahaan'),
        'Bidang_Perusahaan'       => $this->request->getPost('Bidang_Perusahaan'),
        'Nama_Pimpinan_Perusahaan' => $this->request->getPost('Nama_Pimpinan_Perusahaan'),
        'Jabatan'                 => $this->request->getPost('Jabatan'),
        'Kontak_Perusahaan'       => $this->request->getPost('Kontak_Perusahaan'),
        'Email_Perusahaan'        => $this->request->getPost('Email_Perusahaan'),
        'Website_Perusahaan'      => $this->request->getPost('Website_Perusahaan'),
        'Kota_Perusahaan'         => $this->request->getPost('Kota_Perusahaan'),
        'Kode_Pos'                => $this->request->getPost('Kode_Pos'),
        'Alamat_Perusahaan'       => $this->request->getPost('Alamat_Perusahaan'),
        'Tglbulan_Awal'           => $this->request->getPost('Tglbulan_Awal'),
        'Tglbulan_Akhir'          => $this->request->getPost('Tglbulan_Akhir'),
        'Status_Pengajuan'        => $statusPengajuan,
      );

      //simpan data
      $this->pengajuanM->insert($data);

      //setflashdata message
      session()->setFlashdata('message_success', 'Pengajuan berhasil di kirim, tunggu persetujuan dari Admin Hubin !!!');
      return redirect()->to(route_to('pengajuan'));
    } else {
      session()->setFlashdata('message_errors', 'Hallo anda sudah mengajukan pengajuan mohon untuk tunggu balasan dari admin hubin !!!');
      return redirect()->to(route_to('pengajuan'));
    }
  }
  //--------------------------------------------------------------------

  //-------------Reset Password-----------------------------------------
  public function resetPassword($id_user)
  {
    //validasi
    if ($this->validate([
      'Password' => [
        'label' => 'Password',
        'rules'  => 'required|min_length[8]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'min_length'  => '{field} minimal 8 karakter !!!'
        ]
      ],
      'Repassword' => [
        'label' => 'Ketik Password',
        'rules'  => 'required|matches[Password]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'matches'  => '{field} Tidak sama !!!'
        ]
      ],

    ])) {
      //isvalid
      //update
      $this->usersM->save([
        'Id_User'  => $id_user,
        'Password' => password_hash($this->request->getPost('Password'), PASSWORD_DEFAULT),
      ]);

      //setelah reset password remove semua sessionnya
      session()->remove('Email');
      session()->remove('login');
      session()->remove('Password');
      session()->remove('Level');
      session()->setFlashdata('message_info', 'Password anda berhasil di reset, untuk kembali ke dashboard silahkan login dengan password baru anda !!!');
      return redirect()->to(route_to('login'));
    } else {
      //isinvalid
      return redirect()->back()->withInput();
    }
    //--------------------------------------------------------------------



  }
  //----------------EDIT QUOTE------------------------------------------
  public function quoteEdit($nis)
  {
    //update
    $this->siswaM->save([
      'NIS'         => $nis,
      'Quote_Siswa' => $this->request->getPost('Quote_Siswa'),
    ]);

    //redirect to
    session()->setFlashdata('message_success', 'Quote anda berhasil di update !!!');
    return redirect()->back();
  }
  //--------------------------------------------------------------------
}
