<?php

namespace App\Controllers;

use App\Models\authM;
use App\Models\usersM;
use App\Models\roleM;

use phpDocumentor\Reflection\PseudoTypes\True_;

class Auth extends BaseController
{

  public function __construct()
  {
    $this->siswaM = new usersM();
    helper('form');
  }

  //-----------------------LOGIN----------------------------------------
  public function login()
  {
    $data = ['title' => 'Login'];
    return view('auth/v_login', $data);
  }

  public function proses_login()
  {
    $model = new AuthM();
    $table = 'users';
    //jika valid
    $email    = $this->request->getPost('Email');
    $pass     = $this->request->getPost('Password');
    $row      = $model->get_data_login($email, $table);
    // $cekLogin = $model->login($email, $password);


    if ($row == Null) {
      session()->setFlashdata('message_error', 'Username atau Password salah !!!');
      return redirect()->to('/login');
    }
    if ($row and password_verify($pass, $row->Password)) {
      $data = array(
        'login'    => true,
        'Id_User'  => $row->Id_User,
        'Username' => $row->Username,
        'Role'     => $row->Role,
        'NamaRole' => $row->Nama_Role,
        'Foto'     => $row->Foto,
      );
      session()->set($data);

      if ($row->Role == 1) {
        session()->setFlashdata('message_success', 'Berhasil login !!!');
        return redirect()->to('/admin');
      } elseif ($row->Role == 2) {
        session()->setFlashdata('message_success', 'Berhasil login !!!');
        return redirect()->to('/hubin');
      } elseif ($row->Role == 3) {
        $datasiswa = array(
          'siswa' => $this->siswaM->getDataSiswa($email),
        );
        session()->set($datasiswa);
        session()->setFlashdata('message_success', 'Berhasil login !!!');
        return redirect()->to('/siswa');
      }
    }
    session()->setFlashdata('message_error', 'Username atau Password salah !!!');
    return redirect()->to('/login');
  }
  //--------------------------------------------------------------------

  //---------------------REGISTER---------------------------------------
  public function register()
  {
    $roleM = new roleM();

    $data = [
      'title' => 'Register Akun',
      'role'  => $roleM->getRole(),
      'validate' => \Config\Services::validation()
    ];
    return view('auth/v_register', $data);
  }

  public function proses_register()
  {
    //validasi
    if (!$this->validate([
      'Username' => [
        'label' => 'Username',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Email' => [
        'label' => 'Email',
        'rules'  => 'required|Is_unique[users.Email]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'Is_unique'   => '{field} Ini sudah terdaftar !!!',
        ]
      ],
      'Password' => [
        'label' => 'Password',
        'rules'  => 'required|min_length[8]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'min_length'  => '{field} minimal 8 karakter !!!'
        ]
      ],
      'Repassword' => [
        'label' => 'Retype Password',
        'rules'  => 'required|matches[Password]',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'matches'  => '{field} Tidak sama !!!'
        ]
      ],
      'Role' => [
        'label' => 'Role',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ]
    ])) {
      $validasi = \Config\Services::validation();
      return redirect()->to('/register')->withInput()->with('validate', $validasi);
      // return redirect()->to('/register')->withInput(); <== bisa menggunakan cara yang ini
    }

    $data = array(
      'Username'  => $this->request->getPost('Username'),
      'Role'      => $this->request->getPost('Role'),
      'Email'     => $this->request->getPost('Email'),
      'Password'  => password_hash($this->request->getPost('Password'), PASSWORD_DEFAULT),
    );
    $model = new usersM();
    $model->insert($data);
    session()->setFlashdata('message_success', 'Anda berhasil registrasi akun, silahkan login untuk melanjutkan !!!');
    return redirect()->to('/login');
  }
  //--------------------------------------------------------------------

  //--------------------LOGOUT------------------------------------------
  public function logout()
  {
    session()->remove('Email');
    session()->remove('login');
    session()->remove('Password');
    session()->remove('Level');
    // session()->destroy(); jika menggunakan ini maka tidak bisa memparsing setFlashdata
    session()->setFlashdata('message_success', 'Anda berhasil logout !!!');
    return redirect()->to('/');
  }
  //--------------------------------------------------------------------

}
