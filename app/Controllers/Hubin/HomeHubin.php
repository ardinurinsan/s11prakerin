<?php

namespace App\Controllers\Hubin;

use App\Controllers\BaseController;
use App\Models\Hubin\hubinM;
use App\Models\Sadmin\perusM;
use App\Models\Sadmin\siswaM;
use App\Models\Sadmin\jurusanM;
use App\Models\Siswa\pengajuanModel;
use PHPUnit\Util\Json;

class HomeHubin extends BaseController
{

  public function __construct()
  {
    //untuk crud perusahaan
    $this->hubinM = new hubinM();
    $this->perusM = new perusM();

    //untuk crud siswa
    $this->siswaM = new siswaM();
    $this->jurusanM = new jurusanM();

    $this->pengajuanM = new pengajuanModel();
    helper('form');

    $this->userRole  = session()->get('Role');
    $this->userLogin = session()->get('login');
  }

  //----------------V_Index---------------------------------------------
  public function index()
  {
    $data = [
      'title'      => 'Dashboard | Hubin',
      'judul'      => 'Dashboard Admin Hubin',
      'methodName' => 'Dashboard',
      'isi'        => 'pages/hubin/v_index_hubin',
      'jmlh_pengajuan'  => $this->pengajuanM->hitungJumlahPengajuan(),
      'jmlh_siswa'      => $this->siswaM->hitungJumlahSiswa(),
      'jmlh_perusahaan' => $this->perusM->countAllResults(),
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //-----------------V_List_Pengajuan-----------------------------------
  public function listPengajuan()
  {
    $data = [
      'title'      => 'List Pengajuan Tempat Prakerin | Hubin',
      'judul'      => 'List Pengajuan tempat Prakerin',
      'methodName' => 'listpengajuan',
      'pengajuan'  => $this->hubinM->getListDataPengajuan(),
      // 'jmlh_pengajuan_siswa'  => $this->pengajuanM->hitungJumlahPengajuanSiswa(),
      'isi'        => 'pages/hubin/v_list_pengajuan_hubin',
    ];

    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //--------------------------------------------------------------------
  public function update_status()
  {
    $getStatus = $this->request->getPost('id_sp');
    // $getValue  = $this->request->getPost('status');
    $data = array(
      'Id_SP'             => $this->request->getPost('id_sp'),
      'Status_Pengajuan'  => $this->request->getPost('status'),
    );


    if ($this->request->getPost('status') == 2) {
      //ambil nis siswa dari ajax
      $nis = $this->request->getPost('nis');
      //ambil semua data dari nis user
      $rowSiswa = $this->siswaM->getDataSiswaBaris($nis);
      //tambah kan 1 jika status pengajuan nya di tolak
      $statPengajuan = 1;
      $dataS   = array(
        'Jumlah_Pengajuan' => $statPengajuan,
      );
      $this->siswaM->update_data_siswa($dataS, $rowSiswa->NIS);
    }

    // $valueInt = intval($getValue);
    $this->hubinM->updatePengajuan($data, $getStatus);

    $msg = [
      'message_success' => 'Pengajuan berhasil di ubah !!!'
    ];
    echo json_encode($msg);
  }
  //--------------------------------------------------------------------

  /** CRUD PERUSAHAAN */
  //--------------------V_INDEX------------------------------------------
  public function perusahaan()
  {
    $data = [
      'title'      => 'Tabel Master | Perusahaan',
      'judul'      => 'Tabel Perusahaan',
      'methodName' => 'perusahaan',
      'perusahaan' => $this->perusM->getPerus(),
      'isi'        => 'pages/hubin/perusahaan/v_index_perus'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //--------------------CREATE------------------------------------------
  public function perTambah()
  {
    $data = [
      'title'      => 'Perusahaan | Tambah',
      'judul'      => 'Tambah Perusahaan',
      'methodName' => 'perusahaan',
      'perusahaan' => $this->perusM->getPerus(),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/hubin/perusahaan/v_add_perus'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function proses_tambah()
  {
    //validasi
    if ($this->validate([
      'namaperus' => [
        'label' => 'Nama Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'pimperus' => [
        'label' => 'Nama Pimpinan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'jabpimperus' => [
        'label' => 'Jabatan Pimpinan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'alamatperus' => [
        'label' => 'Alamat Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'email' => [
        'label' => 'Email Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'kotperus' => [
        'label' => 'Kota Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'koperus' => [
        'label' => 'Kota Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
    ])) {
      //is valid
      $data = array(
        'Id_Perusahaan'     => $this->request->getPost('Id_Perusahan'),
        'Nama_Perusahaan'   => $this->request->getPost('namaperus'),
        'Nama_Pimprus'      => $this->request->getPost('pimperus'),
        'Jabatan_Pimprus'   => $this->request->getPost('jabpimperus'),
        'Alamat_Perusahaan' => $this->request->getPost('alamatperus'),
        'Email_Perusahaan'  => $this->request->getPost('email'),
        'Website'           => $this->request->getPost('webperus'),
        'Kontak_Perusahaan' => $this->request->getPost('koperus'),
        'Kota_Perusahaan'   => $this->request->getPost('kotperus'),
        'Kode_Pos'          => $this->request->getPost('kotperus'),
      );

      //simpan
      $this->perusM->insert($data);

      //redirect
      session()->setFlashdata('message_success', 'Data perusahaan berhasil di simpan !!!');
      return redirect()->to(route_to('perusahaan_hubin'))->withInput();
    } else {
      return redirect()->to(route_to('create_perus'))->withInput();
    }
  }
  //--------------------------------------------------------------------

  //--------------------UPDATE------------------------------------------
  public function edit($id_perus)
  {
    $data = [
      'title'      => 'Perusahaan | Edit',
      'judul'      => 'Edit Perusahaan',
      'methodName' => 'perusahaan',
      'perusahaan' => $this->perusM->getPerus($id_perus),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/hubin/perusahaan/v_update_perus'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function proses_edit($id_perus)
  {
    //validasi
    if ($this->validate([
      'namaperus' => [
        'label' => 'Nama Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'pimperus' => [
        'label' => 'Nama Pimpinan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'jabpimperus' => [
        'label' => 'Jabatan Pimpinan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'alamatperus' => [
        'label' => 'Alamat Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'email' => [
        'label' => 'Email Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'kotperus' => [
        'label' => 'Kota Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'koperus' => [
        'label' => 'Kota Perusahaan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
    ])) {
      //is valid
      //update
      $this->perusM->save([
        'Id_Perusahaan'     => $id_perus,
        'Nama_Perusahaan'   => $this->request->getPost('namaperus'),
        'Nama_Pimprus'      => $this->request->getPost('pimperus'),
        'Jabatan_Pimprus'   => $this->request->getPost('jabpimperus'),
        'Alamat_Perusahaan' => $this->request->getPost('alamatperus'),
        'Email_Perusahaan'  => $this->request->getPost('email'),
        'Website'           => $this->request->getPost('webperus'),
        'Kontak_Perusahaan' => $this->request->getPost('koperus'),
        'Kota_Perusahaan'   => $this->request->getPost('kotperus'),
        'Kode_Pos'          => $this->request->getPost('kotperus'),
      ]);

      //redirect
      session()->setFlashdata('message_success', 'Data perusahaan berhasil di edit !!!');
      return redirect()->to(route_to('perusahaan_hubin'))->withInput();
    } else {
      return redirect()->to(route_to('proupdateperus'))->withInput();
    }
  }
  //--------------------------------------------------------------------

  //--------------------DELETE------------------------------------------
  public function delete($id_perus)
  {
    $this->perusM->delete($id_perus);
    session()->setFlashdata('message_success', 'Data perusahaan berhasil di hapus !!!');
    return redirect()->to(base_url(route_to('perusahaan_hubin')));
  }
  //--------------------------------------------------------------------


  /** CRUD SISWA */
  //---------------V-INDEX-SISWA----------------------------------------
  public function index_siswa()
  {
    $data = [
      'title'       => 'Tabel Master | Siswa',
      'judul'       => 'Tabel Siswa',
      'methodName'  => 'dataMasterS',
      'siswa'       =>  $this->siswaM->getSiswa(),
      'isi'         => 'pages/hubin/siswa/v_index_siswa'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  //--------------------------------------------------------------------

  //--------------------CREATE------------------------------------------
  public function create_siswa()
  {
    $data = [
      'title'      => 'Siswa | Tambah',
      'judul'      => 'Tambah Data Siswa',
      'methodName' => 'dataMasterS',
      'jurusan'    => $this->jurusanM->getJurusan(),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/hubin/siswa/v_create_siswa'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }

  public function process_save()
  {
    //validasi
    if ($this->validate([
      'NIS' => [
        'label' => 'NIS',
        'rules'  => 'required|is_unique[mst_siswa.NIS]|max_length[10]|numeric',
        'errors' => [
          'required'   => '{field} Tidak boleh kosong !!!',
          'is_unique'  => '{field} Sudah terdaftar !!!',
          'max_length' => '{field} Maximal 10 inputan !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Nama_Siswa' => [
        'label'  => 'Nama Siswa',
        'rules'  => 'required|alpha_space',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'alpha_space' => '{field} Harus berupa huruf!!!',
        ]
      ],
      'Kelas' => [
        'label'  => 'Kelas',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'JK_Siswa' => [
        'label'  => 'Jenis Kelamin',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Id_Jurusan' => [
        'label'  => 'Jurusan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Kontak_Siswa' => [
        'label'  => 'No Kontak',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Alamat' => [
        'label'  => 'Alamat',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],

    ])) {
      //is valid
      $noInputan       = "+62" . $this->request->getPost('Kontak_Siswa') . "";
      $jumlahPengajuan = "1";
      $intPengajuan    = intval($jumlahPengajuan);
      $data = array(
        'NIS'         => $this->request->getPost('NIS'),
        'Nama_Siswa'  => $this->request->getPost('Nama_Siswa'),
        'Kelas'       => $this->request->getPost('Kelas'),
        'JK_Siswa'    => $this->request->getPost('JK_Siswa'),
        'Kontak_Siswa' =>  $noInputan,
        'Id_Jurusan'  => $this->request->getPost('Id_Jurusan'),
        'Alamat'      => $this->request->getPost('Alamat'),
        'Jumlah_Pengajuan'  => $intPengajuan,
      );

      //simpan data
      $this->siswaM->insert($data);

      session()->setFlashdata('message_success', 'Tambah data Siswa berhasil !!!');
      return redirect()->to(route_to('v_siswa'))->withInput();
    } else {
      //is invalid
      return redirect()->to(route_to('v_create_siswa'))->withInput();
    }
  }
  //--------------------------------------------------------------------
  //--------------------UPDATE------------------------------------------
  public function update_siswa($nis)
  {
    $data = [
      'title'      => 'Siswa | Edit',
      'judul'      => 'Edit Data Siswa',
      'methodName' => 'dataMasterS',
      'jurusan'    => $this->jurusanM->getJurusan(),
      'siswa'      => $this->siswaM->getDataSiswaBaris($nis),
      'validation' => \Config\Services::validation(),
      'isi'        => 'pages/hubin/siswa/v_update_siswa'
    ];
    if ($this->userRole != 2 and  $this->userLogin == true) {
      if ($this->userRole == 1) {
        return redirect()->to(base_url('admin'));
      } elseif ($this->userRole == 3) {
        return redirect()->to(base_url('siswa'));
      }
    }
    return view('layout/backend/component/v_wrapper', $data);
  }
  public function proUpdatesiswa($nis)
  {
    //validasi
    if ($this->validate([
      'NIS' => [
        'label' => 'NIS',
        'rules'  => 'required|max_length[10]|numeric',
        'errors' => [
          'required'   => '{field} Tidak boleh kosong !!!',
          'max_length' => '{field} Maximal 10 inputan !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Nama_Siswa' => [
        'label'  => 'Nama Siswa',
        'rules'  => 'required|alpha_space',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'alpha_space' => '{field} Harus berupa huruf!!!',
        ]
      ],
      'Kelas' => [
        'label'  => 'Kelas',
        'rules'  => 'required',
        'errors' => [
          'required'  => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'JK_Siswa' => [
        'label'  => 'Jenis Kelamin',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Id_Jurusan' => [
        'label'  => 'Jurusan',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],
      'Kontak_Siswa' => [
        'label'  => 'No Kontak',
        'rules'  => 'required|numeric',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
          'numeric'    => '{field} Harus berupa angka!!!',
        ]
      ],
      'Alamat' => [
        'label'  => 'Alamat',
        'rules'  => 'required',
        'errors' => [
          'required'    => '{field} Tidak boleh kosong !!!',
        ]
      ],

    ])) {
      //is valid
      $data = array(
        'NIS'         => $this->request->getPost('NIS'),
        'Nama_Siswa'  => $this->request->getPost('Nama_Siswa'),
        'Kelas'       => $this->request->getPost('Kelas'),
        'JK_Siswa'    => $this->request->getPost('JK_Siswa'),
        'Kontak_Siswa' => $this->request->getPost('Kontak_Siswa'),
        'Id_Jurusan'  => $this->request->getPost('Id_Jurusan'),
        'Alamat'      => $this->request->getPost('Alamat'),
      );
      //simpan data
      $this->siswaM->update_data_siswa($data, $nis);


      session()->setFlashdata('message_success', 'Edit data Siswa berhasil !!!');
      return redirect()->to(route_to('v_siswa'))->withInput();
    } else {
      //is invalid
      return redirect()->to(base_url('hubin/siswa/edit/' . $nis))->withInput();
    }
    //--------------------------------------------------------------------
  }

  //--------------------DELETE------------------------------------------
  public function delete_siswa($nis)
  {
    $this->siswaM->deleteSiswa($nis);
    session()->setFlashdata('message_success', 'Data siswa berhasil di hapus !!!');
    return redirect()->to(base_url(route_to('v_siswa')));
  }
  //--------------------------------------------------------------------



}
