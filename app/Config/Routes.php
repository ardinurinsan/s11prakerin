<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

$routes->get('/', 'Pages::index', ['as' => 'homeIndex']);

//router sistem login
$routes->group('/', function ($routes) {
	$routes->add('login', 'auth::login', ['as' => 'login']);
	$routes->add('logout', 'auth::logout', ['as' => 'logout']);
	$routes->add('register', 'auth::register', ['as' => 'register']);
	$routes->add('proses_login', 'auth::proses_login');
	$routes->add('proses_register', 'auth::proses_register');
});

//router role 1 super admin (sesudah login)
$routes->group('admin', function ($routes) {
	$routes->add('/', 'SAdmin\HomeAdmin::index', ['as' => 'admin']);
	$routes->add('maintenance', 'Pages::maintenance', ['as' => 'maintenance_admin']);

	//Users CRUD
	$routes->add('users', 'SAdmin\Users::index', ['as' => 'users']);
	//Create
	$routes->add('users/tambah', 'SAdmin\Users::create', ['as' => 'create_user']);
	$routes->add('users/tambah/proses_simpan', 'SAdmin\Users::proses_simpan', ['as' => 'proses_simpan']);
	//Update
	$routes->get('users/edit/(:segment)', 'SAdmin\Users::update/$1', ['as' => 'update_user']);
	$routes->add('users/process_update/(:num)', 'SAdmin\Users::proses_update/$1', ['as' => 'process_user']);
	//Delete
	$routes->delete('users/(:num)', 'SAdmin\Users::delete/$1', ['as' => 'delete_user']);

	//CRUD MASTER START (Siswa)
	//siswa
	$routes->add('siswa', 'SAdmin\mstSiswa\HomeSiswaM::index', ['as' => 'index_siswa']);
	$routes->add('siswa/create', 'SAdmin\mstSiswa\HomeSiswaM::create', ['as' => 'create_siswa']);
	$routes->add('siswa/process_save', 'SAdmin\mstSiswa\HomeSiswaM::process_save', ['as' => 'process_save_siswa']);
	$routes->get('siswa/edit/(:segment)', 'SAdmin\mstSiswa\HomeSiswaM::update/$1');
	$routes->add('siswa/process_update/(:num)', 'SAdmin\mstSiswa\HomeSiswaM::proses_update_siswa/$1', ['as' => 'process_update_siswa']);
	$routes->delete('siswa/(:num)', 'SAdmin\mstSiswa\HomeSiswaM::delete/$1', ['as' => 'delete_siswa']);
	//CRUD MASTER START (perusahaan)
	//perusahan
	$routes->add('perusahaan', 'SAdmin\mstPerus\HomePerusM::index', ['as' => 'perusahaan_admin']);
	//guru
	$routes->add('guru', 'SAdmin\mstGuru\HomePemguru::index');
	//CRUD MASTER END
});

//router role 2 admin hubin (sesudah login)
$routes->group('hubin', function ($routes) {
	$routes->add('/', 'Hubin\HomeHubin::index', ['as' => 'hubin']);
	$routes->add('list-pengajuan', 'Hubin\HomeHubin::listPengajuan', ['as' => 'list_pengajuan_tmpt_prakerin']);
	$routes->add('list-pengajuan/proses_update_status', 'Hubin\HomeHubin::update_status');
	//CRUD PERUSAHAAN MASTER
	$routes->add('perusahaan', 'Hubin\HomeHubin::perusahaan', ['as' => 'perusahaan_hubin']);
	$routes->add('perusahaan/tambah', 'Hubin\HomeHubin::perTambah', ['as' => 'create_perus']);
	$routes->add('perusahaan/tambah/proses_tambah', 'Hubin\HomeHubin::proses_tambah', ['as' => 'procreate_perus']);
	$routes->get('perusahaan/edit/(:segment)', 'Hubin\HomeHubin::edit/$1', ['as' => 'proupdateperus']);
	$routes->add('perusahaan/process_update/(:segment)', 'Hubin\HomeHubin::proses_edit/$1');
	$routes->delete('perusahaan/(:segment)', 'Hubin\HomeHubin::delete/$1');

	//CRUD SISWA MASTER
	$routes->add('siswa', 'Hubin\HomeHubin::index_siswa', ['as' => 'v_siswa']);
	$routes->add('siswa/tambah', 'Hubin\HomeHubin::create_siswa', ['as' => 'v_create_siswa']);
	$routes->add('siswa/process_save', 'Hubin\HomeHubin::process_save', ['as' => 'proses_simpan_siswa']);
	$routes->get('siswa/edit/(:segment)', 'Hubin\HomeHubin::update_siswa/$1');
	$routes->add('siswa/process_update/(:num)', 'Hubin\HomeHubin::proUpdatesiswa/$1');
	$routes->delete('siswa/(:num)', 'Hubin\HomeHubin::delete_siswa/$1', ['as' => 'del_siswa']);
});


//router role 3 siswa (sesudah login)
$routes->group('siswa',  function ($routes) {
	$routes->add('/', 'Siswa\HomeSiswa::index', ['as' => 'siswa']);
	$routes->add('maintenance', 'Pages::maintenance', ['as' => 'maintenance_siswa']);
	$routes->add('profile', 'Siswa\HomeSiswa::profile', ['as' => 'profile_siswa']);
	//reset password
	$routes->add('profile/edit/(:num)', 'Siswa\HomeSiswa::resetPassword/$1');
	//edit quote
	$routes->add('profile/edit-quote/(:num)', 'Siswa\HomeSiswa::quoteEdit/$1');
	//halaman pengajuan
	$routes->get('pengajuan', 'Siswa\HomeSiswa::pengajuan', ['as' => 'pengajuan']);
	//proses penyimpanan pengajuan
	$routes->add('pengajuan/simpan', 'Siswa\HomeSiswa::proses_save_pengajuan', ['as' => 'pengajuan_simpan']);
});


if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
