<?php

namespace App\Models;

use CodeIgniter\Model;

class roleM extends Model
{
  protected $table      = 'roles';
  protected $primaryKey = 'Id_Role';

  public function getRole($id = false)
  {
    if ($id == false) {
      return $this->findAll();
    }
    return $this->where(['Nama_Role' => $id])->first();
  }
}
