<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthM extends Model
{
  protected $table      = 'users';
  protected $primaryKey = 'id';

  function get_data_login($email, $tbl)
  {
    return $this->db->table($tbl)->join('roles', 'roles.Id_Role=users.Role')->where('Email', $email)->get()->getRow();
  }

  public function login($email, $password)
  {
    return $this->db->table('users')->where([
      'Email'   => $email,
      'Password' => password_verify($password, 'Password'),
    ])->get()->getRowArray();
  }
}
