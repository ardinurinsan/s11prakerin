<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersM extends Model
{
  protected $table      = 'users';
  protected $primaryKey = 'Id_User';
  protected $allowedFields = ['Username', 'Email', 'Password', 'Role', 'Status', 'Foto'];
  protected $useTimestamps = true;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';

  public function __construct()
  {
    $this->db = db_connect();
    $this->builder = $this->db->table($this->table);
  }

  public function getDataUsers($id = false)
  {
    if ($id == false) {
      return $this->builder->join('roles', 'roles.Id_Role=users.Role', 'left')->get()->getResultArray();
    } else {
      return $this->where(['Id_User' => $id])->first();
    }
  }

  public function getDataUserBaris($id_user)
  {
    return $this->where([$this->primaryKey => $id_user])->get()->getRow();
  }

  public function update_data_user($data, $id_user)
  {
    return $this->builder->update($data, ['Id_User' => $id_user]);
  }

  public function deleteUser($id_user)
  {
    return $this->builder->delete(['Id_User' => $id_user]);
  }

  // public function getUserId($email)
  // {
  //   return $this->builder->find('Id_User' => $email);
  // }

  //ambil id user berdasarkan email user(saya pilih email karena email ini bersifat unik)
  public function getDataSiswa($email)
  {
    if ($email) {
      return $this->builder
        ->join('mst_siswa', 'mst_siswa.Id_User=users.Id_User')
        ->join('roles', 'roles.Id_Role=users.Role')
        ->join('jurusan', 'jurusan.Id_Jurusan=mst_siswa.Id_Jurusan')
        ->where('Email', $email)
        ->get()->getRow();
    }

    return $this->where(['Email' => $email])->firs();
  }
}
