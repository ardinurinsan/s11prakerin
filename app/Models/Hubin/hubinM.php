<?php

namespace App\Models\Hubin;

use CodeIgniter\Model;

class hubinM extends Model
{
  protected $table      = 'siswa_pengajuan';
  protected $primaryKey = 'Id_SP';
  protected $allowedFields = ['Id_SP', 'NIS', 'Nama_Perusahaan', 'Nama_Pimpinan_Perusahaan', 'Jabatan', 'Bidang_Perusahaan', 'Email_Perusahaan', 'Kontak_Perusahaan', 'Alamat_Perusahaan', 'Status_Pengajuan', 'Website_Perusahaan', 'Kode_Pos', 'Tglbulan_Awal', 'Tglbulan_Akhir', 'Kota'];
  protected $useTimestamps = true;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';

  public function __construct()
  {
    $this->db = db_connect();
    $this->builder = $this->db->table($this->table);
  }

  public function getListDataPengajuan($id_sp = false)
  {
    if ($id_sp == false) {
      return $this->builder->join('mst_siswa', 'mst_siswa.NIS=siswa_pengajuan.NIS')
        ->join('jurusan', 'jurusan.Id_Jurusan=mst_siswa.Id_Jurusan')
        ->join('users', 'users.Id_User=mst_siswa.Id_User')
        ->get()->getResultArray();
    }
    return $this->where(['NIS' => $id_sp])->get()->getRow();
  }

  public function updatePengajuan($datavalue, $id)
  {
    return $this->builder->update($datavalue, ['Id_SP' => $id]);
  }
}
