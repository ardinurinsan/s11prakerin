<?php

namespace App\Models\Sadmin;

use CodeIgniter\Model;

class jurusanM extends Model
{
  protected $table      = 'jurusan';
  protected $primaryKey = 'Id_Jurusan';

  public function __construct()
  {
    $this->db = db_connect();
    $this->builder = $this->db->table($this->table);
  }

  public function getJurusan($id = false)
  {
    if ($id == false) {
      return $this->builder->get()->getResultArray();
    }
    return $this->where(['Nama_Jurusan' => $id])->first();
  }
}
