<?php

namespace App\Models\Sadmin;

use CodeIgniter\Model;

class siswaM extends Model
{
  protected $table      = 'mst_siswa';
  protected $primaryKey = 'NIS';
  protected $allowedFields = ['NIS', 'Id_Jurusan', 'Id_User', 'Nama_Siswa', 'Kelas', 'Alamat', 'JK_Siswa', 'Kontak_Siswa', 'Jumlah_Pengajuan', 'Quote_Siswa'];
  protected $useTimestamps = true;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';

  public function __construct()
  {
    $this->db = db_connect();
    $this->builder = $this->db->table($this->table);
  }

  public function getSiswa($nis = false)
  {
    if ($nis == false) {
      return $this->builder->join('jurusan', 'jurusan.Id_Jurusan=mst_siswa.Id_Jurusan', 'left')->get()->getResultArray();
    }
    return $this->where(['NIS' => $nis])->first();
  }

  public function getDataSiswaBaris($nis)
  {
    return $this->where([$this->primaryKey => $nis])->get()->getRow();
  }

  public function getDataSiswaLogin($nis)
  {
    return $this->builder->join('users', 'users.Id_User=mst_siswa.Id_User')
      ->where('NIS', $nis)->get();
  }

  //insert data
  public function simpan_data_siswa($data)
  {
    return $this->builder->insert($data);
  }

  //update data
  public function update_data_siswa($data, $nis)
  {
    return $this->builder->update($data, ['NIS' => $nis]);
  }

  public function deleteSiswa($nis)
  {
    return $this->builder->delete(['NIS' => $nis]);
  }

  public function hitungJumlahSiswa()
  {
    return $this->builder->countAllResults();
  }
}
