<?php

namespace App\Models\Sadmin;

use CodeIgniter\Model;

class perusM extends Model
{
  protected $table         = 'mst_perusahaan';
  protected $primaryKey    = 'Id_Perusahaan';
  protected $allowedFields = ['Id_Perusahaan', 'Nama_Perusahaan', 'Nama_Pimprus', 'Alamat_Perusahaan', 'Email_Perusahaan', 'Kontak_Perusahaan', 'Website', 'Kota_Perusahaan', 'Kode_Pos', 'Jabatan_Pimprus'];
  protected $useTimestamps = true;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';

  public function __construct()
  {
    $this->db = db_connect();
    $this->builder = $this->db->table($this->table);
  }

  public function getPerus($id_perus = false)
  {
    if ($id_perus == false) {
      return $this->builder->get()->getResultArray();
    }
    return $this->where(['Id_Perusahaan' => $id_perus])->get()->getRow();
  }
}
