<?php

namespace App\Models\Siswa;

use CodeIgniter\Model;

class pengajuanModel extends Model
{
  protected $table      = 'siswa_pengajuan';
  protected $primaryKey = 'Id_SP';
  protected $allowedFields = ['Id_SP', 'NIS', 'Nama_Perusahaan', 'Nama_Pimpinan_Perusahaan', 'Kota_Perusahaan', 'Jabatan', 'Bidang_Perusahaan', 'Email_Perusahaan', 'Kontak_Perusahaan', 'Alamat_Perusahaan', 'Website_Perusahaan', 'Kode_Pos', 'Tglbulan_Awal', 'Tglbulan_Akhir', 'Kota'];
  protected $useTimestamps = true;
  protected $createdField  = 'created_at';
  protected $updatedField  = 'updated_at';

  public function getDataPengajuan($nis)
  {
    // if ($nis == false) {
    //   return $this->builder->join('mst_siswa', 'mst_siswa.NIS=siswa_pengajuan.NIS')
    //     ->join('jurusan', 'jurusan.Id_Jurusan=mst_siswa.Id_Jurusan')
    //     ->join('users', 'users.Id_User=mst_siswa.Id_User')
    //     ->get()->getResultArray();
    // }
    return $this->where(['NIS' => $nis])->get()->getRow();
  }

  public function getDataPengajuanSama($nis)
  {
    return $this->where(['NIS' => $nis])->get()->getResultArray();
  }

  public function hitungJumlahPengajuan()
  {
    return $this->db->table($this->table)->countAllResults();
  }
}
