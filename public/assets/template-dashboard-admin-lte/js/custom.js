// tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
  $('[data-tooltip="tooltip"]').tooltip();
});


function previewImg() {
  const foto       = document.querySelector('#Foto');
  const fotolabel  = document.querySelector('.custom-file-label');
  const imgPreview = document.querySelector('.img-preview');

  fotolabel.textContent = foto.files[0].name;

  const fileFoto = new FileReader();
  fileFoto.readAsDataURL(foto.files[0]);

  fileFoto.onload  = function (e) {
    imgPreview.src = e.target.result;
  }
}

// sweetalert  sukses
const swal = $('.swal').data('swal');
if (swal) {
  Swal.fire({
    title: 'Berhasil !!!',
    text: swal,
    icon: 'success',
  })
}

// sweetalert  gagal
const error = $('.ggl').data('ggl');
if (error) {
  Swal.fire({
    title: 'Oops !!!',
    text: error,
    icon: 'error',
  })
}

// sweetalert confirm
// $('#setstatus').on('click', function(){
//   Swal.fire({
//     title: 'Apakah anda yakin?',
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Ya!',
//     cancelButtonText: 'Tidak!'
//   }).then((result) => {
//     if (result.isConfirmed) {
//       var id_sp = $(this).attr('data-id');
//       var valstatus = $(this).val();
//       $.ajax({
//         type : 'post',
//         data : {
//           id_sp :id_sp,
//           status: valstatus
//         },
//         dataType : 'json',
//         url : 'list-pengajuan/proses_update_status',
//         success : function(data){
//           console.log(data);
//         }
//       });
  
//       Swal.fire(
//         'Berhasil!',
//         'Penggantian status berhasil.',
//         'success'
//       )
//     }
//   })
// });



// datateble
$(function() {
  $("#tablecrud1").DataTable();
  $('#tablecrud2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });
});



//detail siswa
$(document).on('click', '#btn-detail-siswa', function () {
  $('#nis').text($(this).data('nis'));
  $('#namasis').text($(this).data('namasis'));
  $('#kelas').text($(this).data('kelas'));
  $('#jk').text($(this).data('jk'));
  $('#alamat').text($(this).data('alamat'));
  $('#kontaksis').text($(this).data('kontaksis'));
  $('#jurusan').text($(this).data('jurusan'));
})

//detail list pengajuan 
$(document).on('click', '#btn-detail-listpengajuan', function() {
  $('#namasiswa').text($(this).data('namasiswa'));
  $('#kelassiswa').text($(this).data('kelassiswa'));
  $('#jurusan').text($(this).data('jurusan'));
  $('#jk_siswa').text($(this).data('jk_siswa'));
  $('#kontak_siswa').text($(this).data('kontak_siswa'));
  $('#nama_perusahaan').text($(this).data('nama_perusahaan'));
  $('#nama_pimpinan_perus').text($(this).data('nama_pimpinan_perus'));
  $('#jabatan').text($(this).data('jabatan'));
  $('#emailperus').text($(this).data('emailperus'));
  $('#kontak_perusahaan').text($(this).data('kontak_perusahaan'));
  $('#alamat').text($(this).data('alamat'));
  $('#status').text($(this).data('status'));
  $('#kodepos').text($(this).data('kodepos'));
  $('#tglawal').text($(this).data('tglawal'));
  $('#tglakhir').text($(this).data('tglakhir'));
  $('#kota').text($(this).data('kotaperus'));
  $('#website').text($(this).data('website'));
  $('#nama_bidang_perus').text($(this).data('bidang'));
})
