// tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
  $('[data-tooltip="tooltip"]').tooltip();
});

// sweetalert  sukses
const swal = $('.swal').data('swal');
if (swal) {
  Swal.fire({
    title: 'Berhasil !!!',
    text: swal,
    icon: 'success',
  })
}

// sweetalert  gagal
const error = $('.ggl').data('ggl');
if (error) {
  Swal.fire({
    title: 'Oops !!!',
    text: error,
    icon: 'error',
  })
}
const info = $('.info').data('info');
if (info) {
  Swal.fire({
    title: 'Info !!!',
    text: info,
    icon: 'info',
  })
}


  
// navbar scroll
$(window).scroll(function () {
  $('nav').toggleClass('scrolled', $(this).scrollTop() > 560);
});
$(window).scroll(function () {
  $('nav-link').toggleClass('abc', $(this).scrollTop() > 560);
});

// animation typed
// new Typed('#typed',{
//   strings : ['Hello !!!', 'Selamat datang di website S11Prekerin Kuy','Kamu punya referensi tempat perusahaan?','Ayo login dan ajukan tempat prakerin pilihan mu !!!'],
//   typeSpeed : 100,
//   delaySpeed : 600,
//   loop : true
// });
